#Track Me
This project was made for the component-based software development and web services course.

####Technologies
- Java, JavaEE, Spring
- RESTful, SOAP 
- Javascript, Coffeescript, Google Maps
- Redis, Jdbc

Realized with Agile Methodology, UML 
