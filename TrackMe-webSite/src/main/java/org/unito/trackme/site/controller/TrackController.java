package org.unito.trackme.site.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.unito.trackme.site.model.Track;
import org.unito.trackme.site.model.TrackOperations;
import org.unito.trackme.site.model.User;

/**
 *
 * @author Mattia Bertorello
 */
@Controller
@RequestMapping("/track")
public class TrackController {

    private static final Log log = LogFactory.getLog(TrackController.class);
    @Autowired
    private TrackOperations trackOperation;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newTrack(ModelMap model, HttpServletRequest request) {
        model.put("track", new Track());
        User fromSession = (User) request.getSession().getAttribute("currentUser");
       // log.info("User id from session: " + fromSession.getId());
        return "track/uploadTrack";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createTrack(@ModelAttribute("track") Track track, HttpSession session, BindingResult result) {
        log.info("New Track: " + track.getTitle() + ", " + track.getTransportType() + ", " + track.getGpxPointFile().getOriginalFilename());
        String id;
        try {
            User sessionUser = (User) session.getAttribute("currentUser");
            log.info("User id: " + sessionUser.getId());
            id = trackOperation.createTrackFromGpxFile(track, sessionUser, track.getGpxPointFile().getInputStream());
        } catch (Exception ex) {
            Logger.getLogger(TrackController.class.getName()).log(Level.SEVERE, "Exeption: ", ex);
            result.reject("file", "Parsing error");
            return "track/uploadTrack";
        }
        return "redirect:/track/" + id;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getTrack(@PathVariable(value = "id") String id, ModelMap modelMap,HttpSession session) {
        log.info("Ottengo informazioni traccia con ID = "+id);
        try{
            User sessionUser = (User) session.getAttribute("currentUser");
            log.info("User id: " + sessionUser.getId());   
            Track track = trackOperation.getTrack(id); 
            modelMap.addAttribute("track",track);
        }
        catch(Exception ex){
            //TODO 
            log.info("Exception: "+ex.getMessage());
        }
        return "track/showTrack";
    }
}
