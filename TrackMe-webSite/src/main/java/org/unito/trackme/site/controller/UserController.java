package org.unito.trackme.site.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.unito.trackme.site.model.Track;
import org.unito.trackme.site.model.User;
import org.unito.trackme.site.model.UserOperations;

/**
 *
 * @author Gabriele Mandalari
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private static final Log log = LogFactory.getLog(UserController.class);
    @Autowired
    private UserOperations userOperation;    
    
    @RequestMapping(value = "/track", method = RequestMethod.GET)
    public String getUserTracks(ModelMap model, HttpServletRequest request,HttpSession session) { 
        log.info("Ottengo informazioni sulle tracce dell'utente");
        try{
            User sessionUser = (User) session.getAttribute("currentUser");
            List<Track> list = userOperation.getUserTracks( sessionUser.getId().toString());
            model.addAttribute("trackList", list);            
        }
        catch(Exception ex)
        {}
        return "track/userTracks";
    }
    
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String getUserInfo(ModelMap model, HttpServletRequest request, HttpSession session) {        
        log.info("Ottengo informazioni sull'utente");
        return "info";
    }
}
