/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.site.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Mattia Bertorello
 */
@Controller("indexController")
public class IndexController {

   @RequestMapping("/")
    public String index(ModelMap model) {
          
        model.addAttribute("sigh", "lesigh");
        return "index";
    }
}
