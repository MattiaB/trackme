package org.unito.trackme.site.model;

import java.util.ArrayList;
import java.util.List;
import org.unito.trackme.webservice.AuthenticationException_Exception;
import org.unito.trackme.webservice.TrackMeException_Exception;
import org.unito.trackme.webservice.UsersWebService;

/**
 * Wrapper of the SOAP UsersWebService providing operations over Users.
 */
public class UserOperations {

    private UsersWebService port;

    public UserOperations() {
        port = getPort();
    }

    public User createUser(User formUser) throws TrackMeException_Exception  {
        return fromWebServiceUser(port.createUser(toWebServiceUser(formUser)));
    }

    public User login(User user) throws AuthenticationException_Exception, TrackMeException_Exception  {
        return fromWebServiceUser(port.login(user.getUsername(), user.getPassword()));
    }
    
    public List<Track> getUserTracks(String id) throws TrackMeException_Exception {
        List<org.unito.trackme.webservice.Track> list = port.getTrack(id); 
        return fromWebSeriviceTracks(list);
    }

    private org.unito.trackme.webservice.User toWebServiceUser(User formUser) {
        org.unito.trackme.webservice.User newUser = new org.unito.trackme.webservice.User();
        newUser.setEmail(formUser.getEmail());
        newUser.setUsername(formUser.getUsername());
        newUser.setPassword(formUser.getPassword());
        return newUser;
    }
    
    private List<Track> fromWebSeriviceTracks(List<org.unito.trackme.webservice.Track> webServiceTracks) {
        List<Track> tracks = new ArrayList<Track>(webServiceTracks.size());
        for (org.unito.trackme.webservice.Track webServiceTrack : webServiceTracks) {
            Track track = new Track();
            track.setId(webServiceTrack.getId());
            track.setTitle(webServiceTrack.getTitle());
            track.setTransportType(webServiceTrack.getTransportType());
            track.setPoints(fromWebSerivicePoints(webServiceTrack.getPoints()));
            tracks.add(track);
        }
        return tracks;
    }   
    
    private List<Point> fromWebSerivicePoints(List<org.unito.trackme.webservice.Point> webServicePoints) {
        List<Point> points = new ArrayList<Point>(webServicePoints.size());
        for (org.unito.trackme.webservice.Point webServicePoint : webServicePoints) {
            points.add(new Point(webServicePoint.getLatitude(), webServicePoint.getLongitude(), webServicePoint.getLongitude(), webServicePoint.getTime()));
        }
        return points;
    }
    
    private User fromWebServiceUser(org.unito.trackme.webservice.User webServiceUser) {
        User newUser = new User();
        newUser.setId(webServiceUser.getId());
        newUser.setEmail(webServiceUser.getEmail());
        newUser.setUsername(webServiceUser.getUsername());
        newUser.setToken(webServiceUser.getToken());
        return newUser;        
    }

    private UsersWebService getPort() {
        org.unito.trackme.webservice.UsersWebService_Service service = new org.unito.trackme.webservice.UsersWebService_Service();
        return service.getUsersWebServicePort();
    }
}
