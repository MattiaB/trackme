/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.site.model;

/**
 *
 * @author Gabriele Mandalari
 */
public class Challenge {
    private String title;
    private String transportType;
    private long id;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
       
    
}
