package org.unito.trackme.site.model;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.unito.trackme.site.controller.TrackController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * GPX File Parser.
 */
public class GpxFileParser {

    private static final Log log = LogFactory.getLog(TrackController.class);

    /**
     * Parse the given gpx file and returns its list of points.
     * 
     * @param xmlFile the gpx file that has to be parsed.
     * @return the list of the GPS locations contained in the gpx file.
     * @throws Exception if the gpx file is not well formed.
     */
    public static List<Point> parser(InputStream xmlFile) throws Exception {
        
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        List<Point> points;
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document gpxDocument = db.parse(xmlFile);
        NodeList list = gpxDocument.getElementsByTagName("trkpt");
        points = new ArrayList<Point>(list.getLength());
        for (int i = 0; i < list.getLength(); i++) {
            Node elem = list.item(i);
            String latitude = ((Element) elem).getAttribute("lat");
            String longitude = ((Element) elem).getAttribute("lon");
            String elevation = ((Element) elem).getElementsByTagName("ele").item(0).getTextContent();
            String time = ((Element) elem).getElementsByTagName("time").item(0).getTextContent();
            log.info("latitude: " + latitude + "\tlongitude: " + longitude + "\televation: " + elevation + "\ttime: " + time);
            points.add(new Point(latitude, longitude, elevation, time));
        }
        
        return points;
    
    }
}
