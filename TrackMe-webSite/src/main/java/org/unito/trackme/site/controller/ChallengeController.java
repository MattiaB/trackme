package org.unito.trackme.site.controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.unito.trackme.site.model.Challenge;
import org.unito.trackme.site.model.ChallengeOperations;
import org.unito.trackme.site.model.User;

/**
 *
 * @author Mattia Bertorello
 */
@Controller
@RequestMapping("/challenge")
public class ChallengeController {

    private static final Log log = LogFactory.getLog(ChallengeController.class);
    @Autowired
    private ChallengeOperations challengeOperations;
    
     @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getChallenges(ModelMap modelMap,HttpSession session) {
        log.info("Ottengo informazioni sulle sfide");
        try{
            User sessionUser = (User) session.getAttribute("currentUser");
            log.info("User id: " + sessionUser.getId());   
            List<Challenge> challengeList = challengeOperations.getChallenges(sessionUser.getId().toString());
            modelMap.addAttribute("challengeList",challengeList);            
        }
        catch(Exception ex){
            //TODO 
            log.info("Exception: "+ex.getMessage());
        }
        return "challenge/listChallenge";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getChallenge(@PathVariable(value = "id") String id, ModelMap modelMap, HttpSession session) {        
        return "challenge/show";
    }
}
