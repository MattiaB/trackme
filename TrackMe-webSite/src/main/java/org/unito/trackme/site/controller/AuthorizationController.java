package org.unito.trackme.site.controller;

import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.unito.trackme.site.model.User;
import org.unito.trackme.site.model.UserOperations;

/**
 *
 * @author Mattia Bertorello
 */
@Controller
@SessionAttributes({"user"})
public class AuthorizationController {

    private static final Log log = LogFactory.getLog(AuthorizationController.class);
    @Autowired
    public UserOperations userOperations;

    @RequestMapping(value = "/sign-up", method = RequestMethod.GET)
    public String signUpNew(ModelMap model) {
        model.put("newUser", new User());
        return "sign-up";
    }
    // @ModelAttribute("newUser") deve essere uguale al commandName

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public String createUser(@ModelAttribute("newUser") User user, BindingResult result) {
        log.info("Username:" + user.getUsername() + "Password:" + user.getPassword() + "Email:" + user.getEmail() + "Confirm Password:" + user.getConfirmPassword());
        if (user.getPassword().equals(user.getConfirmPassword())) {

            try { // Call Web Service Operation
                userOperations.createUser(user);
            } catch (Exception ex) {
                result.reject("user", ex.getMessage());
                return "/sign-up";
            }
        } else {
            result.reject("password", "Le password non sono uguali");
            return "/sign-up";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(ModelMap model) {
        model.put("loginUser", new User());
        return "login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String loginPost(@ModelAttribute("loginUser") User user, BindingResult result, SessionStatus status,HttpSession session) {
        log.info("Username:" + user.getUsername() + "Password:" + user.getPassword());
        try { // Call Web Service Operation
            User loginUser = userOperations.login(user);
            result.getModel().remove("user");
            session.setAttribute("currentUser", loginUser);
            status.setComplete();
            log.info("Login avvenuto con successo: " + loginUser.getEmail() + " id: " + loginUser.getId());
        } catch (Exception ex) {
            log.info("Errore nel login: " + ex.getMessage());
            result.reject("loginUser", ex.getMessage());
            return "login";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    private String logout(SessionStatus status,HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            log.info("user logout: " + ((User) session.getAttribute("currentUser")).getId());
            session.removeAttribute("currentUser");
            session.invalidate();
            status.setComplete();
        } else {
            log.info("Log out without login");
        }
        return "redirect:/";
    }
}
