package org.unito.trackme.site.model;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.unito.trackme.webservice.TrackMeException_Exception;
import org.unito.trackme.webservice.TracksWebService;

/**
 * Wrapper of the SOAP TracksWebService providing operations over Tracks.
 */
public class TrackOperations {

    private TracksWebService port;

    public TrackOperations() {
        port = getPort();
    }

    public String createTrackFromGpxFile(Track data, User currentUser, InputStream file) throws Exception {
        data.setUser(currentUser);
        org.unito.trackme.webservice.Track newTrack = toWebServiceTrack(data);
        String idTrack = port.createTrack(newTrack, currentUser.getId()).getId().toString();
        
        List<Point> points = GpxFileParser.parser(file);

        List<org.unito.trackme.webservice.Point> webServicePoints = toWebServicePoints(points);

        port.putPoints(idTrack, webServicePoints);
        port.endRealTimeTrack(idTrack);
        return idTrack;
    }

    public Track getTrack(String id) throws TrackMeException_Exception {
        org.unito.trackme.webservice.Track webSerivceTrack = port.getCompleteTrack(id);
        return fromWebSeriviceTrack(webSerivceTrack);
    }

    private List<Point> fromWebSerivicePoints(List<org.unito.trackme.webservice.Point> webServicePoints) {
        List<Point> points = new ArrayList<Point>(webServicePoints.size());
        for (org.unito.trackme.webservice.Point webServicePoint : webServicePoints) {
            points.add(new Point(webServicePoint.getLatitude(), webServicePoint.getLongitude(), webServicePoint.getLongitude(), webServicePoint.getTime()));
        }
        return points;
    }

    private List<org.unito.trackme.webservice.Point> toWebServicePoints(List<Point> points) {
        List<org.unito.trackme.webservice.Point> webServicePoints = new ArrayList<org.unito.trackme.webservice.Point>(points.size());
        for (Point point : points) {
            org.unito.trackme.webservice.Point webServicePoint = new org.unito.trackme.webservice.Point();
            webServicePoint.setLatitude(point.getLatitude());
            webServicePoint.setLongitude(point.getLongitude());
            webServicePoint.setElevation(point.getElevation());
            webServicePoint.setTime(point.getTime());
            webServicePoints.add(webServicePoint);
        }
        return webServicePoints;

    }

    private Track fromWebSeriviceTrack(org.unito.trackme.webservice.Track webSerivicetrack) {
        Track track = new Track();
        track.setTitle(webSerivicetrack.getTitle());
        track.setTransportType(webSerivicetrack.getTransportType());
        track.setPoints(fromWebSerivicePoints(webSerivicetrack.getPoints()));
        return track;
    }

    private org.unito.trackme.webservice.Track toWebServiceTrack(Track track) {
        Logger.getLogger(TrackOperations.class.getName()).log(Level.SEVERE, null, "User id toWebServiceTrack:" + track.getUser().getId());
        org.unito.trackme.webservice.Track webServiceTrack = new org.unito.trackme.webservice.Track();
        webServiceTrack.setTitle(track.getTitle());
        webServiceTrack.setTransportType(track.getTransportType());
        org.unito.trackme.webservice.User user = new org.unito.trackme.webservice.User();
        user.setId(track.getUser().getId());
        return webServiceTrack;
    }

    private TracksWebService getPort() {
        org.unito.trackme.webservice.TracksWebService_Service service = new org.unito.trackme.webservice.TracksWebService_Service();
        return service.getTracksWebServicePort();
    }
}
