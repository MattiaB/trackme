package org.unito.trackme.site.model;

import java.util.List;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class Track {

    private String title;
    private String transportType;
    private long id;
    private CommonsMultipartFile gpxPointFile;
    private List<Point> points;
    private User user;
    
    public Track() {
    }

    public Track(int id,String title, String transportType, List<Point> points) {
        this.title = title;
        this.id = id;
        this.transportType = transportType;
        this.points = points;
    }
    

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public CommonsMultipartFile getGpxPointFile() {
        return gpxPointFile;
    }

    public void setGpxPointFile(CommonsMultipartFile gpxPointFile) {
        this.gpxPointFile = gpxPointFile;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
  
}
