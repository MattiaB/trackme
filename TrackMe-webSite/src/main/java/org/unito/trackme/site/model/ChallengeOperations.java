/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.site.model;

import java.util.ArrayList;
import java.util.List;
import org.unito.trackme.webservice.TrackMeException_Exception;
import org.unito.trackme.webservice.UsersWebService;

/**
 *
 * @author Gabriele Mandalari
 */
public class ChallengeOperations {
    
    private UsersWebService port;

    public ChallengeOperations() {
        port = getPort();
    }
 
    public List<Challenge> getChallenges(String id) throws TrackMeException_Exception  {
        List<org.unito.trackme.webservice.Challenge> webSerivceChallenges= port.getChallenges(id);
        return fromWebSeriviceChallenge(webSerivceChallenges);
    }
    
    private List<Challenge> fromWebSeriviceChallenge(List<org.unito.trackme.webservice.Challenge> webSerivceChallenges) {
        List<Challenge> challenges = new ArrayList<Challenge>(webSerivceChallenges.size());
        for (org.unito.trackme.webservice.Challenge webSerivceChallenge : webSerivceChallenges) {
            Challenge challenge = new Challenge();
            challenge.setId(webSerivceChallenge.getId());
            challenge.setTitle(webSerivceChallenge.getTitle());
            challenge.setTransportType(webSerivceChallenge.getTransportType());
            challenge.setDescription(webSerivceChallenge.getDescription());
            challenges.add(challenge);
        }
        return challenges;
    }   
    
    private UsersWebService getPort() {
        org.unito.trackme.webservice.UsersWebService_Service service = new org.unito.trackme.webservice.UsersWebService_Service();
        return service.getUsersWebServicePort();
    }
}
