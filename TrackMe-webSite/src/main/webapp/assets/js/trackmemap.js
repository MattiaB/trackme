//var map;
//function initialize() {
//    var mapOptions = {
//        zoom: 8,
//        center: new google.maps.LatLng(-34.397, 150.644),
//        mapTypeId: google.maps.MapTypeId.ROADMAP
//    };
//    map = new google.maps.Map(document.getElementById('map_canvas'),
//        mapOptions);
//}
//
//google.maps.event.addDomListener(window, 'load', initialize);
//token = {
//    token: "ef34c73a-e7ca-4e7c-a5e5-17d378c1f8e8"
//};
//
//var request = $.ajax({
//    url: "/api/track/2/points",
//    type: "GET",
//    data: token,
//    dataTypeString: "json"
//});
//
//// callback handler that will be called on failure
//request.fail(function (jqXHR, textStatus, errorThrown){
//    // log the error to the console
//    console.error("The following error occured: "+textStatus, errorThrown);
//});
//
//// callback handler that will be called regardless
//// if the request failed or succeeded
//request.always(function () {
//    // reenable the inputs
//    console.log("always work!");
//});

// Restituisce i due Bounds agli estremi per fare fitBounds()
google.maps.Polyline.prototype.getExtremeBounds = function() {
    var bounds = new google.maps.LatLngBounds();
    var path = this.getPath();     
    for (var i = 0; i < path.getLength(); i++) {
        bounds.extend(path.getAt(i));
    }
    var extremeBounds = new google.maps.LatLngBounds();
    extremeBounds.extend(bounds.getNorthEast());
    extremeBounds.extend(bounds.getSouthWest());
    return extremeBounds;
}
// Variabile che contiene tutto il codice riguardante Trackme
var Trackme = (function(){
    
    return {
        // Esegue il run del javascript attaccando gli eventi al controller
        run: function(){
            var path = window.location.pathname;
            var controller = undefined;
            if(path.match("/track/")){
                console.log("track");
                controller = this.trackController;
            }else if(path.match("/challenge/")){
                console.log("challenge");
                controller = this.challengeController;
            }
            controller.attachEvents(this.model,this.view);
            console.log("Run!");
        
        }
    };

})();
// Inizializza un oggetto che contiene le "classi" del modello
Trackme.model = (function(){
    console.log("Init Model");
    return {};
})();
Trackme.model.apiCall = function(options){
    var token = {
        token: document.getElementById('userToken').value
    };
    options = $.extend({
        type: "GET",
        data: token,
        dataTypeString: "json"
    }, options);
    option.url = "/api/"+options.url;
    return $.ajax(options);
}
// Inizializzazione della classe Track contiene i dati di una traccia
// per creare un nuovo oggetto bisogna eseguirla come una funzione 
// @param {String} trackId    Contiene l'id della traccia.
// @return {String}  Un oggetto che contiene tutte le informazioni della traccia
Trackme.model.track = function(trackId){
    console.log("New Track with thit URL: "+trackId);
    token = {
        token: document.getElementById('userToken').value
    };
    // Variabile che contiene i valori della traccia e anche tutte le callback 
    // legate all'update dei valori dei punti nel caso sia real time
    var track = (function(){
        var arrayCallback = [];
        var value = undefined;
        // Funzione che chiama tutte le callback assocciate all'update
        var updateFunction = function(){
            for (var i = 0; i < arrayCallback.length; i++){
                arrayCallback[i](value);
            }  
        }
        return {
            // Restituisce l'oggetto che contiene i dati della traccia
            get: function(){
                return value;
            },
            // Esegue l'update di tutta la traccia
            update: function(track){
                value = track;
                updateFunction();
            },
            // Esegue l'update dei punti della traccia
            updatePoints: function(points){
                value.points = points;
                updateFunction();
            },
            // Aggancia una callback che verrà chiamata quando viene 
            // aggiornata la traccia
            addCallbackWhenUpdate: function(callback){
                arrayCallback.push(callback);  
            }
        }
    })();
    // Richiesta ajax per chiedere tutte le informazioni sulla traccia trackId
    var requestCompleteTrack = $.ajax({
        url: "/api/track/"+trackId,
        type: "GET",
        data: token,
        dataTypeString: "json"
    });
    requestCompleteTrack.done(function (response, textStatus, jqXHR){
        // Aggiornamento traccia 
        track.update(response);
        // Nel caso fosse in real time viene attivato un timeout per aggiornare 
        // i punti
        if(track.get().real_time === true){
            var timeoutID = undefined;
            // Chiamata ajax per aggiornare i punti della traccia
            var ajaxPointUpdate = function(){ 
                var request = $.ajax({
                    url: "/api/track/"+trackId+"/points",
                    type: "GET",
                    data: token,
                    dataTypeString: "json"
                });
                request.done(function (response, textStatus, jqXHR){
                    // Aggiornamento dei punti della traccia
                    track.updatePoints(response.points);
                    // Cancellazione del timeout precedente 
                    window.clearTimeout(timeoutID)
                    // Creazione di un nuovo timeout 
                    timeoutID = window.setTimeout( ajaxPointUpdate, 1000 );
                });
            };
            // Creazione di un timeout per rigenerare i punti
            timeoutID = window.setTimeout( ajaxPointUpdate, 1000 );
        
        
        }
    });
    
    return {
        // Permette di agganciare una callback per essere informati quando i 
        // punti vengono aggiornati
        whenPointAreUpdate: function(callback){
            track.addCallbackWhenUpdate(callback);
        },
        // Restituzione di tutta la traccia quando è disponibile
        get: function(callback){
            if(track.get() === undefined){
                requestCompleteTrack.done(function (response, textStatus, jqXHR){
                    callback(response);
                });
            }else{
                callback(track.get());
            }
        }
    
    }
}
Trackme.model.challenge = function(chellengeId){
    token = {
        token: document.getElementById('userToken').value
    };
    var challenge = undefined;
    var requestCompleteChallenge = $.ajax({
        url: "/api/challenge/"+chellengeId,
        type: "GET",
        data: token,
        dataTypeString: "json"
    });
    requestCompleteChallenge.done(function (response, textStatus, jqXHR){
        challenge = response
    });
}
// Controller che gestisce gli eventi e crea callback per legare l'aggiornamento
// del view con l'aggiornamento del model
Trackme.trackController = (function(){
    console.log("Init Track Controller");
    return {
        attachEvents: function(model, view){
            google.maps.event.addDomListener(window, 'load', function(){
                // Disegna la mappa
                view.drawMap();
                // Estrazione del trackId per il model
                var path = window.location.pathname;
                var trackId = path.substring(path.lastIndexOf('/')+1,path.length);
                // Creazione della traccia
                var track = model.track(trackId);
                // Creazione del Marker di Maps 
                var marker = view.drawMarker({
                    latitude:0, 
                    longitude:0
                });
                // Quando c'è un update dei punti
                track.whenPointAreUpdate(function(track){
                    // Inserisci il marker nell'ultimo punto del percorso
                    view.updateMarker(track.points[track.points.length-1], marker);
                    // Disegna la traccia sulla mappa
                    view.drawTrackPoints(track.points);
                });
                // Quando la traccia è pronta 
                track.get(function(track){
                    // Creazione della slide 
                    $('.slider').slider({
                        min: 0,
                        max: track.points.length-1,
                        step: 1,
                        value: track.points.length-1,
                        tooltip: 'show'
                    // Quando l'utente fa slide
                    }).on('slide', function(ev){
                        // Nel caso la traccia fosse real time allora non si 
                        // modifica il marker
                        if(track.real_time == false){
                            view.updateMarker(track.points[ev.value], marker);
                        }
                    });
                });
            
            });
        }
    };
})();
Trackme.challengeController = (function(){
    console.log("Init Challenge Controller");
    return {
        attachEvents: function(model, view){
            google.maps.event.addDomListener(window, 'load', function(){
                // Disegna la mappa
                view.drawMap();
                // Estrazione del trackId per il model
                var path = window.location.pathname;
                var challengeId = path.substring(path.lastIndexOf('/')+1,path.length);
                model.challenge(challengeId);
            });
        }
    }
})();
// View permette di visualizzare i dati 
Trackme.view = (function(){
    console.log("Init View");
    var map;
    // Converte un punto in Google Maps Point
    var convertMyPointInMapsPoint = function(point){
        return new google.maps.LatLng(parseFloat(point.latitude), parseFloat(point.longitude));
    }
    // Converte un array di punti in un array di Google Maps Points 
    var convertToMapsPoints = function(trackMePoints){
        var newMapsPointsArray = [];
        for(var i = 0; i < trackMePoints.length; i++){
            newMapsPointsArray.push(convertMyPointInMapsPoint(trackMePoints[i]))
        }
        return newMapsPointsArray;
    }
    return {
        // Inizializza la mappa su il div id=map_canvas
        drawMap: function(){
            var mapOptions = {
                zoom: 3,
                center: new google.maps.LatLng(0, -180),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map_canvas'),
                mapOptions);
        },
        // Disegna un array di punti sulla mappa e esegue lo zoom con fitBounds
        drawTrackPoints: function(points){
            var pathCoordinates = convertToMapsPoints(points);
            var polyline = new google.maps.Polyline({
                path: pathCoordinates,
                strokeColor: "#FF0000",
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            
            polyline.setMap(map);
            // Esegue lo zoom sul polyline
            map.fitBounds(polyline.getExtremeBounds());
        },
        // Disegna il marker in un punto 
        drawMarker: function(point){
            var marker = new google.maps.Marker({
                map: map,
                position: convertMyPointInMapsPoint(point)
            });
            return marker;
        },
        // Aggiorna il marker sulla mappa in un nuovo punto
        updateMarker: function(newPoint, marker){
            marker.setPosition(convertMyPointInMapsPoint(newPoint));
        }
    };
})();
Trackme.run();
//Trackme.prototype = {
//        
//    createMaps: function(){
//        getPoints()
//    },
//    convertPointInGoogleMapsPoint: function(pointArray){
//        var newGMPointArray = [];
//        for(var i = 0; i < pointArray.length; i++){
//            newGMPointArray.push(new google.maps.LatLng(parseFloat(pointArray[i].latitude), parseFloat(pointArray[i].longitude)))
//        }
//        return newGMPointArray;
//    }
//}


//// callback handler that will be called on success
//request.done(function (response, textStatus, jqXHR){
//    // log a message to the console
//    console.log("Hooray, it worked!");
//    console.log(response);
//    var trackmeFunction = new Trackme();
//    var pathCoordinates = trackmeFunction.convertPointInGoogleMapsPoint(response.points);
//    trackmeFunction.createMaps();
//    console.log(pathCoordinates);
//    var path = new google.maps.Polyline({
//        path: pathCoordinates,
//        strokeColor: "#FF0000",
//        strokeOpacity: 1.0,
//        strokeWeight: 2
//    });
//    
//    var latlngbounds = new google.maps.LatLngBounds( );
//    for ( var i = 0; i < pathCoordinates.length; i++ ) {
//        latlngbounds.extend( pathCoordinates[ i ] );
//    }
//    map.fitBounds( latlngbounds );
//    path.setMap(map);
//});
