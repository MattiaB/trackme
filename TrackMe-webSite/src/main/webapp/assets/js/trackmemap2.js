// Generated by CoffeeScript 1.6.2
(function() {
  var Api, CallbackQueue, ChallengeController, ChallengeView, Challlenge, Track, TrackController, TrackRealTime, TrackView, View, distanceBetweenTwoPoint, run, toMiniteOrHour, typeIsArray,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  TrackController = (function() {
    function TrackController(trackId) {
      google.maps.event.addDomListener(window, 'load', function() {
        var request, view;

        view = new View();
        request = new Api().request({
          url: "track/" + trackId
        });
        return request.done(function(response) {
          var track, trackview;

          track = new TrackRealTime(response);
          view.initializeSlide({
            max: track.get().points.length - 1,
            value: track.get().points.length - 1
          });
          trackview = new TrackView(track, view);
          trackview.drawMarker();
          return trackview.initializeRealTime();
        });
      });
    }

    return TrackController;

  })();

  ChallengeController = (function() {
    function ChallengeController(challengeId) {
      google.maps.event.addDomListener(window, 'load', function() {
        var request, view;

        view = new View();
        request = new Api().request({
          url: "challenge/" + challengeId
        });
        return request.done(function(response) {
          var challenge;

          challenge = new Challlenge(response);
          return new ChallengeView(challenge, view);
        });
      });
    }

    return ChallengeController;

  })();

  run = function() {
    var path, separatedUrl;

    path = window.location.pathname;
    separatedUrl = path.match(/trackme\/(.*)\/(\d*)/);
    if (separatedUrl[1] === "track") {
      return new TrackController(separatedUrl[2]);
    } else if (separatedUrl[1] === "challenge") {
      console.log("sono una challenge " + separatedUrl[2]);
      return new ChallengeController(separatedUrl[2]);
    }
  };

  run();

  google.maps.Polyline.prototype.getExtremeBounds = function() {
    var bounds, extremeBounds, i, length, path;

    bounds = new google.maps.LatLngBounds();
    path = this.getPath();
    length = path.getLength();
    i = 0;
    while (i < length) {
      bounds.extend(path.getAt(i));
      i += 1;
    }
    extremeBounds = new google.maps.LatLngBounds();
    extremeBounds.extend(bounds.getNorthEast());
    extremeBounds.extend(bounds.getSouthWest());
    return extremeBounds;
  };

  distanceBetweenTwoPoint = function(lat1, lon1, lat2, lon2) {
    var R, a, c, d, dLat, dLon, deg2rad;

    deg2rad = function(deg) {
      return deg * (Math.PI / 180);
    };
    R = 6371;
    dLat = deg2rad(lat2 - lat1);
    dLon = deg2rad(lon2 - lon1);
    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    d = R * c;
    return d;
  };

  Api = (function() {
    var token;

    function Api() {}

    token = {
      token: document.getElementById('userToken').value
    };

    Api.prototype.request = function(options) {
      options = $.extend({
        type: "GET",
        data: token,
        dataTypeString: "json"
      }, options);
      options.url = "/api/" + options.url;
      return $.ajax(options);
    };

    return Api;

  })();

  CallbackQueue = (function() {
    function CallbackQueue(object) {
      this.object = object;
      this.queue = [];
    }

    CallbackQueue.prototype.addCallback = function(callback) {
      return this.queue.push(callback);
    };

    CallbackQueue.prototype.callAll = function() {
      var callback, _i, _len, _ref, _results;

      _ref = this.queue;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        callback = _ref[_i];
        _results.push(callback(this.object));
      }
      return _results;
    };

    return CallbackQueue;

  })();

  Track = (function() {
    function Track(data) {
      this.data = data;
      this._updateDistance();
      this.queueCallback = new CallbackQueue(this);
    }

    Track.prototype.get = function() {
      return this.data;
    };

    Track.prototype.set = function(data) {
      this.data = data;
      this._updateDistance();
      return this.queueCallback.callAll();
    };

    Track.prototype._updateDistance = function() {
      var beforePoint, distance, firstPoint, hoursTotalTime, lastPoint, point, points, toHours, totalTime, _i, _len;

      points = this.data.points;
      beforePoint = points[0];
      this.data.totalDistance = 0;
      for (_i = 0, _len = points.length; _i < _len; _i++) {
        point = points[_i];
        distance = distanceBetweenTwoPoint(beforePoint.latitude, beforePoint.longitude, point.latitude, point.longitude);
        point.distance = distance;
        this.data.totalDistance = this.data.totalDistance + distance;
        beforePoint = point;
      }
      firstPoint = new Date(points[0].time);
      lastPoint = new Date(points[points.length - 1].time);
      totalTime = Math.abs(firstPoint - lastPoint);
      this.data.totalTime = totalTime;
      toHours = 60 * 60 * 1000;
      hoursTotalTime = totalTime / toHours;
      return this.data.averageSpeed = this.data.totalDistance / hoursTotalTime;
    };

    Track.prototype.callMeWhenDataAreUpdated = function(callback) {
      return this.queueCallback.addCallback(callback);
    };

    return Track;

  })();

  TrackRealTime = (function(_super) {
    __extends(TrackRealTime, _super);

    function TrackRealTime(data, callbackRequest) {
      var that, timeoutID, updatePoints;

      this.data = data;
      if (callbackRequest == null) {
        callbackRequest = function(c) {};
      }
      TrackRealTime.__super__.constructor.call(this, this.data);
      that = this;
      if (this.data.real_time === true) {
        timeoutID = void 0;
        updatePoints = function() {
          var request;

          request = new Api().request({
            url: "track/" + that.data.id + "/points"
          });
          callbackRequest(request);
          return request.done(function(response) {
            var track;

            track = that.get();
            track.points = response.points;
            that.set(track);
            window.clearTimeout(timeoutID);
            return timeoutID = window.setTimeout(updatePoints, 1000);
          });
        };
        timeoutID = window.setTimeout(updatePoints, 1000);
      }
    }

    return TrackRealTime;

  })(Track);

  Challlenge = (function() {
    function Challlenge(data) {
      var allPoints, auxArray, definitiveAllPoints, first, point, prec, track, tracksArray, _i, _j, _k, _len, _len1, _len2, _ref, _ref1;

      this.data = data;
      this.data.main_track = new Track({
        points: this.data.main_points
      });
      tracksArray = [];
      allPoints = [];
      _ref = this.data.tracks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        track = _ref[_i];
        tracksArray.push(new Track(track));
        first = track.points[0];
        _ref1 = track.points;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          point = _ref1[_j];
          point.track_id = track.id;
          point.time = new Date(point.time);
          point.difference = Math.abs(first.time - point.time);
          allPoints.push(point);
        }
      }
      this.data.tracks = tracksArray;
      allPoints.sort(function(a, b) {
        return a.difference - b.difference;
      });
      console.log(allPoints);
      definitiveAllPoints = [];
      prec = allPoints[0];
      auxArray = [];
      for (_k = 0, _len2 = allPoints.length; _k < _len2; _k++) {
        point = allPoints[_k];
        if (point.difference !== prec.difference) {
          definitiveAllPoints.push(auxArray);
          auxArray = [];
        }
        auxArray.push(point);
        prec = point;
      }
      console.log(definitiveAllPoints);
      this.data.allPoints = definitiveAllPoints;
    }

    Challlenge.prototype.get = function() {
      return this.data;
    };

    Challlenge.prototype.getMaxPoints = function() {
      return this.data.allPoints.length;
    };

    Challlenge.prototype.getPointAt = function(index) {
      return this.data.allPoints[index];
    };

    return Challlenge;

  })();

  typeIsArray = function(value) {
    return value && typeof value === 'object' && value instanceof Array && typeof value.length === 'number' && typeof value.splice === 'function' && !(value.propertyIsEnumerable('length'));
  };

  toMiniteOrHour = function(intValue) {
    var date, stringDate;

    date = new Date(intValue);
    stringDate = '0 s';
    if (date.getSeconds()) {
      stringDate = date.getSeconds() + ' s';
    }
    if (date.getMinutes()) {
      stringDate = date.getMinutes() + ' m ' + stringDate;
    }
    if (date.getHours() - 1) {
      stringDate = (date.getHours() - 1) + ' h ' + stringDate;
    }
    return stringDate;
  };

  View = (function() {
    function View() {
      this.map = this.drawMap();
    }

    View.prototype.convertToMapsPoint = function(point) {
      return new google.maps.LatLng(parseFloat(point.latitude), parseFloat(point.longitude));
    };

    View.prototype.convertToMapsPoints = function(points) {
      var mapsPoints, point, _i, _len;

      mapsPoints = [];
      for (_i = 0, _len = points.length; _i < _len; _i++) {
        point = points[_i];
        mapsPoints.push(this.convertToMapsPoint(point));
      }
      return mapsPoints;
    };

    View.prototype.drawMap = function() {
      var mapOptions;

      mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(0, -180),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      return new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    };

    View.prototype.drawMarker = function(point, options) {
      options = $.extend({
        map: this.map,
        position: this.convertToMapsPoint(point)
      }, options);
      return new google.maps.Marker(options);
    };

    View.prototype.updateMarker = function(marker, newPoint) {
      return marker.setPosition(this.convertToMapsPoint(newPoint));
    };

    View.prototype.initializeSlide = function(options) {
      options = $.extend({
        min: 0,
        step: 1,
        tooltip: 'show'
      }, options);
      return this.slide = $('.slider').slider(options);
    };

    return View;

  })();

  TrackView = (function() {
    function TrackView(track, view, options) {
      var difference, first, that, thisPoint;

      this.track = track;
      this.view = view;
      this.points = this.track.get().points;
      if (this.view.slide) {
        first = new Date(this.points[0].time);
        thisPoint = new Date(this.points[this.points.length - 1].time);
        difference = Math.abs(first - thisPoint);
        this.view.slide[0].previousSibling.lastChild.innerText = toMiniteOrHour(difference);
      }
      that = this;
      this.drawTrackPoints(options);
      this.updataData();
      this.track.callMeWhenDataAreUpdated(function() {
        that.points = that.track.get().points;
        that.view.updateMarker(that.marker, that.points[that.points.length - 1]);
        that.drawTrackPoints(options);
        return that.updataData();
      });
    }

    TrackView.prototype.initializeRealTime = function() {
      var that;

      that = this;
      if (this.track.get().real_time === false) {
        return this.view.slide.on('slide', function(ev) {
          var difference, first, thisPoint;

          first = new Date(that.points[0].time);
          thisPoint = new Date(that.points[ev.value].time);
          difference = Math.abs(first - thisPoint);
          ev.currentTarget.previousSibling.lastChild.innerText = toMiniteOrHour(difference);
          return that.view.updateMarker(that.marker, that.points[ev.value]);
        });
      }
    };

    TrackView.prototype.updataData = function() {
      var averageSpeed, myRound, totalDistance, totalTime;

      if ($('#average_speed').length > 0) {
        myRound = function(number) {
          return Math.round(number * 100) / 100;
        };
        totalTime = toMiniteOrHour(this.track.get().totalTime);
        averageSpeed = myRound(this.track.get().averageSpeed);
        totalDistance = myRound(this.track.get().totalDistance);
        $('#average_speed')[0].innerText = averageSpeed + ' km/h';
        $('#total_distance')[0].innerText = totalDistance + ' km';
        return $('#total_time')[0].innerText = totalTime;
      }
    };

    TrackView.prototype.drawMarker = function() {
      return this.marker = this.view.drawMarker(this.points[this.points.length - 1]);
    };

    TrackView.prototype.drawTrackPoints = function(options) {
      var mapsPoints, polyline;

      if (options == null) {
        options = {};
      }
      mapsPoints = this.view.convertToMapsPoints(this.points);
      options = $.extend({
        path: mapsPoints,
        strokeColor: "#FF0000",
        strokeOpacity: 1.0,
        strokeWeight: 2
      }, options);
      polyline = new google.maps.Polyline(options);
      polyline.setMap(this.view.map);
      return this.view.map.fitBounds(polyline.getExtremeBounds());
    };

    return TrackView;

  })();

  ChallengeView = (function() {
    function ChallengeView(challenge, view) {
      var difference, length, that, track, trackView, viewMainTrack, _i, _len, _ref;

      this.challenge = challenge;
      this.view = view;
      challenge = this.challenge.get();
      viewMainTrack = new TrackView(challenge.main_track, this.view, {
        strokeColor: "#00008B",
        strokeOpacity: 0.3,
        strokeWeight: 20
      });
      this.trackViewsArray = [];
      _ref = challenge.tracks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        track = _ref[_i];
        trackView = new TrackView(track, this.view);
        trackView.drawMarker();
        this.trackViewsArray[track.get().id] = trackView;
      }
      length = this.challenge.getMaxPoints();
      this.view.initializeSlide({
        max: length - 1,
        value: length - 1
      });
      difference = this.challenge.getPointAt(this.challenge.getMaxPoints() - 1)[0].difference;
      this.view.slide[0].previousSibling.lastChild.innerText = toMiniteOrHour(difference);
      that = this;
      this.view.slide.on('slide', function(ev) {
        return that.updateAllMarkerAt(ev.value, ev);
      });
    }

    ChallengeView.prototype.updateAllMarkerAt = function(value, ev) {
      var point, points, trackView, _i, _len, _results;

      points = this.challenge.getPointAt(value);
      ev.currentTarget.previousSibling.lastChild.innerText = toMiniteOrHour(points[0].difference);
      _results = [];
      for (_i = 0, _len = points.length; _i < _len; _i++) {
        point = points[_i];
        trackView = this.trackViewsArray[point.track_id];
        _results.push(this.view.updateMarker(trackView.marker, point));
      }
      return _results;
    };

    return ChallengeView;

  })();

}).call(this);
