google.maps.Polyline.prototype.getExtremeBounds = ->
  bounds = new google.maps.LatLngBounds()
  path = this.getPath()
  length = path.getLength()
  i = 0
  while i < length
    bounds.extend(path.getAt(i))
    i += 1

  extremeBounds = new google.maps.LatLngBounds()
  extremeBounds.extend(bounds.getNorthEast())
  extremeBounds.extend(bounds.getSouthWest())
  return extremeBounds

distanceBetweenTwoPoint = (lat1,lon1,lat2,lon2) ->
  deg2rad = (deg) ->
    return deg * (Math.PI/180)
  R = 6371 # Radius of the earth in km
  dLat = deg2rad(lat2-lat1)  # deg2rad below
  dLon = deg2rad(lon2-lon1);
  a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2)
  c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  d = R * c # Distance in km
  return d;

class Api
  token =
    token: document.getElementById('userToken').value

  request: (options) ->
    options = $.extend({
      type: "GET",
      data: token,
      dataTypeString: "json"
    }, options)
    options.url = "/api/#{options.url}"
    $.ajax(options)

class CallbackQueue
  constructor: (@object)->
    @queue = []

  addCallback: (callback) ->
    @queue.push callback

  callAll: ->
    for callback in @queue
      callback(@object)

class Track
  constructor: (@data) ->
    this._updateDistance()
    @queueCallback = new CallbackQueue(this)

  get: -> @data
  set: (data) ->
    @data = data
    this._updateDistance()
    @queueCallback.callAll()

  _updateDistance: () ->
    points = @data.points
    beforePoint = points[0]
    @data.totalDistance = 0
    for point in points
      distance = distanceBetweenTwoPoint(beforePoint.latitude, beforePoint.longitude,
                                          point.latitude, point.longitude)
      point.distance = distance
      @data.totalDistance =  @data.totalDistance + distance
      beforePoint = point
    firstPoint = new Date(points[0].time)
    lastPoint = new Date(points[points.length-1].time)
    totalTime = Math.abs(firstPoint - lastPoint)
    @data.totalTime = totalTime
    toHours = 60 * 60 * 1000
    hoursTotalTime = totalTime / toHours
    @data.averageSpeed =  @data.totalDistance / hoursTotalTime

  callMeWhenDataAreUpdated: (callback) -> @queueCallback.addCallback(callback)

class TrackRealTime extends Track

  constructor: (@data, callbackRequest = (c) ->) ->
    super @data
    that = this
    if @data.real_time == true
      timeoutID = undefined
      updatePoints =  ->
        request = new Api().request({url: "track/#{that.data.id}/points"})
        callbackRequest(request)
        request.done (response)->
          track = that.get()
          track.points = response.points
          that.set(track)
          window.clearTimeout(timeoutID)
          timeoutID = window.setTimeout(updatePoints, 1000);

      timeoutID = window.setTimeout(updatePoints, 1000);


class Challlenge
  constructor: (@data) ->
    @data.main_track = new Track(
        {points: @data.main_points}
      )
    tracksArray = []
    allPoints = []
    for track in @data.tracks
      tracksArray.push new Track(track)
      # Ipotizzo che i punti siano già ordinati
      first = track.points[0]
      for point in track.points
        point.track_id = track.id
        point.time = new Date(point.time)
        # Differenza dal primo
        point.difference = Math.abs(first.time - point.time)
        allPoints.push point

    @data.tracks = tracksArray

    allPoints.sort( (a,b)-> a.difference - b.difference)
    console.log allPoints

    definitiveAllPoints = []
    prec = allPoints[0]
    auxArray = []
    for point in allPoints
      if(point.difference != prec.difference)
          definitiveAllPoints.push auxArray
          auxArray = []
      auxArray.push point
      prec = point




    console.log definitiveAllPoints
    @data.allPoints = definitiveAllPoints


  get: -> @data

  getMaxPoints: ->
    @data.allPoints.length

  getPointAt: (index) ->
    @data.allPoints[index]