class TrackController
  constructor: (trackId) ->
    google.maps.event.addDomListener window, 'load', ->
      view = new View()
      request = new Api().request({url: "track/#{trackId}"})
      request.done (response)->
        track = new TrackRealTime(response)
        view.initializeSlide({
          max: track.get().points.length - 1,
          value: track.get().points.length - 1,
        })
        trackview = new TrackView(track,view)
        trackview.drawMarker()
        trackview.initializeRealTime()

class ChallengeController
  constructor: (challengeId) ->
    google.maps.event.addDomListener window, 'load', ->
      view = new View()
      request = new Api().request({url: "challenge/#{challengeId}"})
      request.done (response)->
        challenge = new Challlenge(response)
        new ChallengeView(challenge,view)

run = ->
  path = window.location.pathname
  separatedUrl = path.match(/trackme\/(.*)\/(\d*)/)
  if(separatedUrl[1] == "track")
    new TrackController(separatedUrl[2])
  else if(separatedUrl[1] == "challenge")
    console.log "sono una challenge #{separatedUrl[2]}"
    new ChallengeController(separatedUrl[2])

run()