typeIsArray = ( value ) ->
  value and
  typeof value is 'object' and
  value instanceof Array and
  typeof value.length is 'number' and
  typeof value.splice is 'function' and
  not ( value.propertyIsEnumerable 'length' )

toMiniteOrHour = (intValue) ->
  date = new Date(intValue)
  stringDate = '0 s'
  if(date.getSeconds())
    stringDate = date.getSeconds() + ' s'
  if(date.getMinutes())
    stringDate = date.getMinutes() + ' m ' + stringDate
  if(date.getHours()-1)
    stringDate = (date.getHours()-1) + ' h ' + stringDate
  return stringDate

class View
  constructor: () ->
    @map = this.drawMap()

  convertToMapsPoint: (point)->
    new google.maps.LatLng(parseFloat(point.latitude), parseFloat(point.longitude))

  convertToMapsPoints: (points) ->
    mapsPoints = []
    for point in points
      mapsPoints.push this.convertToMapsPoint(point)
    mapsPoints

  drawMap: ->
    mapOptions =
      zoom: 3
      center: new google.maps.LatLng(0, -180)
      mapTypeId: google.maps.MapTypeId.ROADMAP
    new google.maps.Map(document.getElementById('map_canvas'), mapOptions)

  drawMarker: (point, options) ->
    options = $.extend({
      map: @map,
      position: this.convertToMapsPoint(point)
    }, options)
    new google.maps.Marker(options)

  updateMarker: (marker, newPoint) ->
    marker.setPosition(this.convertToMapsPoint(newPoint))

  initializeSlide: (options) ->
    options = $.extend({
      min: 0,
      step: 1,
      tooltip: 'show'
    },options)
    @slide = $('.slider').slider(options)

class TrackView
  constructor: (@track, @view, options) ->
    @points = @track.get().points
    if @view.slide
      first = new Date(@points[0].time)
      thisPoint = new Date(@points[@points.length-1].time)
      difference = Math.abs(first - thisPoint)
      @view.slide[0].previousSibling.lastChild.innerText = toMiniteOrHour(difference)
    that = this

    this.drawTrackPoints(options)
    this.updataData()
    @track.callMeWhenDataAreUpdated ->
      that.points = that.track.get().points
      that.view.updateMarker(that.marker, that.points[that.points.length - 1])
      that.drawTrackPoints(options)
      that.updataData()

  initializeRealTime: ->
    that = this
    if(@track.get().real_time == false)
      @view.slide.on 'slide', (ev)->
        first = new Date(that.points[0].time)
        thisPoint = new Date(that.points[ev.value].time)
        difference = Math.abs(first - thisPoint)
        ev.currentTarget.previousSibling.lastChild.innerText = toMiniteOrHour(difference)
        that.view.updateMarker(that.marker, that.points[ev.value])

  updataData: ->
    if($('#average_speed').length > 0)
      myRound = (number) ->
        return Math.round(number*100)/100
      totalTime = toMiniteOrHour(@track.get().totalTime)
      averageSpeed = myRound(@track.get().averageSpeed)
      totalDistance = myRound(@track.get().totalDistance)
      $('#average_speed')[0].innerText = averageSpeed + ' km/h'
      $('#total_distance')[0].innerText = totalDistance + ' km'
      $('#total_time')[0].innerText = totalTime



  drawMarker: ->
    @marker = @view.drawMarker @points[@points.length-1]

  drawTrackPoints: (options = {}) ->
    mapsPoints = @view.convertToMapsPoints(@points)
    options = $.extend({
      path: mapsPoints,
      strokeColor: "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 2
    }, options)
    polyline = new google.maps.Polyline(options)
    polyline.setMap @view.map
    # Esegue lo zoom sul polyline
    @view.map.fitBounds(polyline.getExtremeBounds())

class ChallengeView
  constructor: (@challenge, @view)->
    challenge = @challenge.get()
    viewMainTrack = new TrackView(challenge.main_track, @view,{
      strokeColor: "#00008B",
      strokeOpacity: 0.3,
      strokeWeight: 20
    })
    @trackViewsArray = []
    for track in challenge.tracks
      trackView = new TrackView(track, @view)
      trackView.drawMarker()
      @trackViewsArray[track.get().id] = trackView

    length = @challenge.getMaxPoints()
    @view.initializeSlide({
      max: length - 1,
      value: length - 1,
    })
    difference = @challenge.getPointAt(@challenge.getMaxPoints()-1)[0].difference
    @view.slide[0].previousSibling.lastChild.innerText = toMiniteOrHour(difference)
    that = this
    @view.slide.on 'slide', (ev)->
      that.updateAllMarkerAt(ev.value, ev)

  updateAllMarkerAt: (value, ev)->
    points = @challenge.getPointAt(value)
    ev.currentTarget.previousSibling.lastChild.innerText = toMiniteOrHour(points[0].difference)
    for point in points
      trackView = @trackViewsArray[point.track_id]
      @view.updateMarker(trackView.marker, point)
