<%-- 
    Document   : info
    Created on : 6-apr-2013, 15.49.11
    Author     : Lele
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<tags:layout>
    <tags:menu />
    <div class="pagination-centered">       
          <div class="thumbnail">
                  <img data-src="holder.js/300x200" alt="300x200" style="width: 250px; height: 200px;" src="<spring:url value="/assets/img/profile.png" htmlEscape="true"/>">
                  <div class="caption">
                    <h3>Username: <c:out value="${currentUser.username}"/></h3>            
                    <h3>Email: <c:out value="${currentUser.email}"/></h3>                   
                  </div>
         </div>
    </div>
</tags:layout>