<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<spring:url value="/sign-up" var="signupUrl" htmlEscape="true"/>
<spring:url value="/login" var="login" htmlEscape="true"/>
<spring:url value="/user/track" var="track" htmlEscape="true"/>
<spring:url value="/challenge/" var="challenge" htmlEscape="true"/>
<tags:layout>
    <tags:menu />
    <div class="pagination-centered">
        <img  style="width: 391px; height: 153px;" src="<spring:url value="/assets/img/logo.png" htmlEscape="true"/>">
         <c:if test="${currentUser.username == null}">
            <p><a class="btn btn-primary" href="${signupUrl}">Sign up and get started</a></p>
         </c:if>
        </br>
        <div class="row-fluid">
            <ul class="thumbnails">
              <li class="span4">
                <div class="thumbnail">
                  <img data-src="holder.js/300x200" alt="300x200" style="width: 300px; height: 200px;" src="<spring:url value="/assets/img/map.jpg" htmlEscape="true"/>">
                  <div class="caption">
                    <h3>Real time tracking</h3>
                    <p>TrackMe offre ai propri utenti la possibilita' di registrare i propri percorsi GPS e di caricarli sul nostro portale.</p>
                    <c:if test="${currentUser.username != null}">
                    <p><a href="${track}" class="btn btn-primary">Tracks</a>
                    </c:if>
                    <c:if test="${currentUser.username == null}">
                    <p><a href="${login}" class="btn btn-primary">Tracks</a>
                    </c:if>    
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img data-src="holder.js/300x200" alt="300x200" style="width: 300px; height: 200px;" src="<spring:url value="/assets/img/challenge.png" htmlEscape="true"/>">
                  <div class="caption">
                    <h3>Challenges</h3>
                    <p>Sfida gli altri utenti! Vedi sei sei il più veloce, confrontati con gli altri nei diversi percorsi presenti.</p>
                    <c:if test="${currentUser.username != null}">
                    <p><a href="${challenge}" class="btn btn-primary">Challenges</a>
                    </c:if>
                    <c:if test="${currentUser.username == null}">
                    <p><a href="${login}" class="btn btn-primary">Challenges</a> 
                    </c:if>
                  </div>
                </div>
              </li>
              <li class="span4">
                <div class="thumbnail">
                  <img data-src="holder.js/300x200" alt="300x200" style="width: 300px; height: 200px;" src="<spring:url value="/assets/img/friends.jpg" htmlEscape="true"/>">
                  <div class="caption">
                    <h3>Friends</h3>
                    <p>Crea rapporti di amicizia con i nostri utenti, sarà più facile trovarli per sfidarli e confrontarsi.</p>
                    <p><a href="#" class="btn btn-primary">Friends</a>
                  </div>
                </div>
              </li>
            </ul>
          </div>       
    </div>
</tags:layout>