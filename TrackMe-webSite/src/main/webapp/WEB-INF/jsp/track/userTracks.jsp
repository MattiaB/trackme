
<%@page contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<tags:layout>
    <tags:menu />
    <table class="table">
              <thead>
                <tr>
                  <th></th>
                  <th>Transport</th>
                  <th>Title</th>
                </tr>
              </thead>
              <tbody>
              <c:forEach items="${trackList}" var="track" varStatus="status">
                <tr>
                  <td align="center">${status.count}</td>  
                  <c:if test="${track.transportType eq 'Bicicletta'}">
                  <td><img  style="width: 130px; height: 50px;" src="<spring:url value="/assets/img/bicicletta.png" htmlEscape="true"/>"></td> 
                  </c:if>
                  <c:if test="${track.transportType eq 'A piedi'}">
                  <td><img  style="width: 50px; height: 50px;" src="<spring:url value="/assets/img/walk.png" htmlEscape="true"/>"></td> 
                  </c:if>
                  <td>${track.title}</td>                   
                  <td>
                      <form action="<c:url value="/track/${track.id}"/>">
                          <input class="btn-info" type="submit" value="Show">
                     </form>                   
                  </td>
                </tr>
              </c:forEach>
              </tbody>
    </table>
</tags:layout>
