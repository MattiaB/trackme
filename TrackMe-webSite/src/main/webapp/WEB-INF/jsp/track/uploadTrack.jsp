<%-- 
    Document   : uploadTrack
    Created on : 31-Dec-2012, 11:16:48
    Author     : Mattia Bertorello
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<spring:url value="/track/create" var="formUrl" htmlEscape="true"/>
<tags:layout>
    <tags:menu />
    <div class="row">
        <div class="span6 offset3">
            <form:form class="form-horizontal" method="post" action="${formUrl}" commandName="track" enctype="multipart/form-data" > 

                <div class="control-group">
                    <label class="control-label" for="inputTitle">Title</label>
                    <div class="controls">
                        <form:input path="title" id="inputTitle" placeholder="Title" required="required" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputTransportType">Transport Type</label>
                    <div class="controls">
                        <form:select path="transportType" id="inputTransportType" placeholder="Transport Type" required="required">
                            <form:option value="Bicicletta" label="Bicicletta" />
                            <form:option value="A piedi" label="A piedi" />
                        </form:select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputGpxPoint">Gpx Point</label>
                    <div class="controls">
                        <form:input path="gpxPointFile" id="inputGpxPoint" type="file" placeholder="Gpx Track"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn">Upload</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</tags:layout>