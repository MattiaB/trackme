<%@page contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<tags:layout>
    <tags:menu />
    <input id="userToken" type="hidden" name="userToken" value="${currentUser.token}">
    <div id="map_canvas"></div>
    <div class="slider slider-horizontal" id="map_slider" style="width: 400px;"></div>
</tags:layout>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsy7w8QmT4e-R9QiExupDg2ha3VbvEvnU&sensor=false"></script>
<script type="text/javascript" src='<spring:url value="/assets/js/trackmemap2.js" htmlEscape="true"/>'></script>
<div class="pagination-centered">
    <table class="table">
        <thead>
            <tr>
            <th></th>
            <th>Transport</th>
            <th>Title</th>
            </tr>
        </thead>
        <tbody>              
            <tr>
            <td align="center">${status.count}</td>  
            <c:if test="${track.transportType eq 'Bicicletta'}">
                <td><img  style="width: 130px; height: 50px;" src="<spring:url value="/assets/img/bicicletta.png" htmlEscape="true"/>"></td> 
                </c:if>
                <c:if test="${track.transportType eq 'A piedi'}">
                <td><img  style="width: 50px; height: 50px;" src="<spring:url value="/assets/img/walk.png" htmlEscape="true"/>"></td> 
                </c:if>
            <td>${track.title}</td>                                    
            </tr>
        </tbody>
    </table>
    <table class="table">
        <thead>
            <tr>
            <th>Total Time</th>
            <th>Average Speed</th>
            <th>Total Distance</th>
            </tr>
        </thead>
        <tbody>              
            <tr>
            <td id="total_time"></td> 
            <td id="average_speed" ></td>
            <td id="total_distance"></td>                                    
            </tr>
        </tbody>
    </table>
</div>