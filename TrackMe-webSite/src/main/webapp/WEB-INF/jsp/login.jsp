<%-- 
    Document   : login
    Created on : 18-Dec-2012, 14:42:46
    Author     : Mattia Bertorello
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<spring:url value="/" var="home" htmlEscape="true"/>
<tags:layout>
    <div class="row">
        <div class="pagination-centered header">
            <h1><a href="${home}">Track Me</a></h1> 
        </div>
    </div>
    <spring:bind path="loginUser">
        <c:if test="${status.error}">
            <c:forEach var="errorMessage" items="${status.errorMessages}">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <h4>Oh snap!</h4>
                    ${errorMessage}
                </div>
            </c:forEach>
        </c:if>
    </spring:bind>
    <div class="row">
        <div class="span6 offset3">
            <form:form class="form-horizontal" method="POST" action='login' commandName="loginUser" > 
                <div class="control-group">
                    <label class="control-label" for="inputEmail">Username/email</label>
                    <div class="controls">
                        <form:input path="username" id="inputEmail" placeholder="Username/email"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Password</label>
                    <div class="controls">
                        <form:password path="password" id="inputPassword" placeholder="Password"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox"> Remember me
                        </label>
                        <button type="submit" class="btn">Log me in</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</tags:layout>
