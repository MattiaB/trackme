<%-- 
    Document   : script
    Created on : 18-Dec-2012, 14:59:55
    Author     : Mattia Bertorello
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript" src='<spring:url value="/assets/js/jquery-1.8.3.min.js" htmlEscape="true"/>'></script>
<script type="text/javascript" src='<spring:url value="/assets/js/bootstrap.min.js" htmlEscape="true"/>'></script>
<script type="text/javascript" src='<spring:url value="/assets/js/bootstrap-slider.js" htmlEscape="true"/>'></script>
