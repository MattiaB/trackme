<%-- 
    Document   : error
    Created on : 28-Dec-2012, 17:47:27
    Author     : Mattia Bertorello
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="info"%>
<%@attribute name="success"%>
<%@attribute name="block"%>
<%@attribute name="error"%>

<c:if test="${info != null}">
    <div class="alert alert-info">
        ${info}
    </div>
</c:if>
<c:if test="${success != null}">
    <div class="alert alert-success">
        ${success}
    </div>
</c:if>
<c:if test="${block != null}">
    <div class="alert alert-block">
        ${block}
    </div>
</c:if>
<c:if test="${error != null}">
    <c:forEach var="errorMessage" items="${error}">
        <div class="alert alert-error">
            ${errorMessage}
        </div>
    </c:forEach>
</c:if>