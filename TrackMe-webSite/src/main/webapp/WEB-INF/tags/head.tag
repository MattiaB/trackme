<%-- 
    Document   : head
    Created on : 18-Dec-2012, 14:58:00
    Author     : Mattia Bertorello
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>Track Me</title>
    <link rel="stylesheet" type="text/css" href='<spring:url value="/assets/css/bootstrap.min.css" htmlEscape="true"/>'/>
    <link rel="stylesheet" type="text/css" href='<spring:url value="/assets/css/trackme.css" htmlEscape="true"/>'/>
    <link rel="stylesheet" type="text/css" href='<spring:url value="/assets/css/slider.css" htmlEscape="true"/>'/>
</head>