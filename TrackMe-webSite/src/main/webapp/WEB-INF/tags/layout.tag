<%-- 
    Document   : layout
    Created on : 18-Dec-2012, 10:20:18
    Author     : Mattia Bertorello
--%>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%-- The list of normal or fragment attributes can be specified here: 
<%@attribute name="message"%>
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
    <tags:head/>
    <body>
        <div class="container">
            <jsp:doBody/>
        </div>
    </body>
</html>
<tags:scripts/>