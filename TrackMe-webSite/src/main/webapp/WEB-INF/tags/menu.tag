<%-- 
    Document   : menu
    Created on : 31-Dec-2012, 11:40:26
    Author     : Mattia Bertorello
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:url value="/" var="home" htmlEscape="true"/>
<spring:url value="/login" var="login" htmlEscape="true"/>
<spring:url value="/logout" var="logout" htmlEscape="true"/>
<spring:url value="/track/new" var="upload" htmlEscape="true"/>
<spring:url value="/challenge/" var="challenge" htmlEscape="true"/>
<spring:url value="/user/track" var="track" htmlEscape="true"/>
<spring:url value="/user/info" var="info" htmlEscape="true"/>
<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="${home}">Track Me</a>
        <ul class="nav">
            <li class="active"><a href="${home}">Home</a></li>
            <li class="divider-vertical"></li>
            <c:if test="${currentUser.username == null}">
                <li><a href="${login}">Log in</a></li>
            </c:if>
            <c:if test="${currentUser.username != null}">
                <li><a href="${track}">My Tracks</a></li>            
                <li><a href="${challenge}">Challenges</a></li>            
                <li><a href="${upload}">Upload</a></li>
                <li><a href="${info}"><c:out value="${currentUser.username}" /></a></li>            
                <li><a href="${logout}">Log Out</a></li>
            </c:if>
        </ul>
    </div>
</div>