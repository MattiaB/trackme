/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.

package org.unito.trackme.restful;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unito.trackme.util.JsonUtil;

/**
 *
 * @author Mattia Bertorello

public class UserResourceTest {

  
    public UserResourceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testClassicBehavior() throws Exception {
        System.out.println("AllMethods");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UserResource userService = (UserResource) container.getContext().lookup("java:global/classes/UserResource");
        TrackResource trackService = (TrackResource) container.getContext().lookup("java:global/classes/TrackResource");

        String username = "mario";
        String password = "test";

        Map<String, String> newUserMap = new HashMap<String, String>();
        newUserMap.put("username", username);
        newUserMap.put("password", password);

        userService.createUser(toJson(newUserMap));
        String loginResult = userService.login(toJson(newUserMap));

        Map<String, String> loginResultMap = toMap(loginResult);
        String token = loginResultMap.get("token");
        String userId = loginResultMap.get("id");

        String userInfoJson = userService.getUserInfo(userId, token);
        assertEquals("Le user info devono essere uguali", loginResult, userInfoJson);

        Map<String, String> newTrackMap = new HashMap<String, String>();
        newTrackMap.put("userId", userId);
        newTrackMap.put("title", "prova");
        String newTrackResult = trackService.newTrack(token, toJson(newTrackMap));
        String getTracksResult = userService.getTracks(userId, token);

        List<Map> userTracksMap = JsonUtil.fromJson(getTracksResult, List.class);
        assertEquals("La traccia restituita deve essere la stessa memorizzata", (String) toMap(newTrackResult).get("id"), (String) userTracksMap.get(0).get("id"));

        String trackId = (String) toMap(newTrackResult).get("id");
        String points = "[{lan:21.3,lon:32.3,elev:321,time:\"dsa\"}]";
        trackService.putPointsInTrack(trackId, token, points);
        trackService.endTrack(trackId, token);
        String completeTrackResult = trackService.getCompleteTrackInfo(trackId, token);
        Map completeTrackResultMap = toMap(completeTrackResult);
        assertEquals("La traccia deve avere tutti i punti", points, completeTrackResultMap.get("points"));

        container.close();
    }

    private Map toMap(String jsonString) {
        return JsonUtil.fromJson(jsonString, HashMap.class);
    }

    private String toJson(Map map) {
        return JsonUtil.toJson(map);
    }
}
 */