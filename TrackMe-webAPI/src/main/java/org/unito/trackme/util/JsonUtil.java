/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.util;

import com.google.gson.Gson;

/**
 *
 * @author Mattia Bertorello
 */
public class JsonUtil {
    public static String toJson(Object source){
        return new Gson().toJson(source);
    }
    public static <T> T fromJson(String json, Class<T> classOfT){
       return new Gson().fromJson(json, classOfT);
    }
}
