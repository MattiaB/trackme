package org.unito.trackme.webservice;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.unito.trackme.entities.Challenge;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.User;
import org.unito.trackme.exception.AuthenticationException;
import org.unito.trackme.exception.TrackMeException;
import org.unito.trackme.managements.UsersManagement;

/**
 * SOAP WebService exposing actions over Users.
 */
@WebService(serviceName = "UsersWebService")
public class UsersWebService {
    @EJB
    private UsersManagement ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Web Service > Add Operation"

    @WebMethod(operationName = "login")
    public User login(@WebParam(name = "username") String username, @WebParam(name = "password") String password) throws TrackMeException, AuthenticationException {
        return ejbRef.login(username, password);
    }

    @WebMethod(operationName = "getUser")
    public User getUser(@WebParam(name = "id") String id) throws TrackMeException {
        return ejbRef.getUser(id);
    }

    @WebMethod(operationName = "createUser")
    public User createUser(@WebParam(name = "newUser") User newUser) throws TrackMeException {
        return ejbRef.createUser(newUser);
    }

    @WebMethod(operationName = "getTrack")
    public List<Track> getTrack(@WebParam(name = "id") String id) throws TrackMeException {
        return ejbRef.getTracks(id);
    }

    @WebMethod(operationName = "getChallenges")
    public List<Challenge> getChallenges(@WebParam(name = "id") String id) throws TrackMeException {
        return ejbRef.getChallenges(id);
    }   
}
