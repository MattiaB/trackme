package org.unito.trackme.webservice;

import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.unito.trackme.entities.Point;
import org.unito.trackme.entities.Track;
import org.unito.trackme.exception.TrackMeException;
import org.unito.trackme.managements.TrackManagement;

/**
 * SOAP WebService exposing actions over Tracks.
 */
@WebService(serviceName = "TracksWebService")
public class TracksWebService {
    @EJB
    private TrackManagement ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Web Service > Add Operation"

    @WebMethod(operationName = "getCompleteTrack")
    public Track getCompleteTrack(@WebParam(name = "trackId") String trackId) throws TrackMeException {
        return ejbRef.getCompleteTrack(trackId);
    }

    @WebMethod(operationName = "getPoints")
    public List<Point> getPoints(@WebParam(name = "trackId") String trackId) throws TrackMeException {
        return ejbRef.getPoints(trackId);
    }

    @WebMethod(operationName = "createTrack")
    public Track createTrack(@WebParam(name = "newTrack") Track newTrack, @WebParam(name = "ownerId") Long ownerId) {
        return ejbRef.createTrack(newTrack, ownerId);
    }

    @WebMethod(operationName = "putPoints")
    public void putPoints(@WebParam(name = "trackId") String trackId, @WebParam(name = "points") Collection<Point> points) throws TrackMeException {
        ejbRef.putPoints(trackId, points);
    }

    @WebMethod(operationName = "endRealTimeTrack")
    public void endRealTimeTrack(@WebParam(name = "trackId") String trackId) throws TrackMeException {
        ejbRef.endRealTimeTrack(trackId);
    }
   
}
