package org.unito.trackme.restful;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.beans.Authentication;
import org.unito.trackme.entities.Challenge;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.User;
import org.unito.trackme.exception.TrackMeException;
import org.unito.trackme.managements.ChallengeManagement;
import org.unito.trackme.managements.TrackManagement;
import org.unito.trackme.util.JsonUtil;

/**
 * RESTful WebService exposing some actions over Challenges.
 * Further informations about methods and available actions
 * can be found in the TrackMe WebService API Specifications document.
 */
@Path("challenge")
@Stateless
public class ChallengeResource {

    private static final Logger logger = LogManager.getLogger(ChallengeResource.class);
    private static final String keyException = "error";
    private static final String invalidKey = "{\"" + keyException + "\":\"string\"}";
    private static final String invalidKeyReplacement = "string";
    private static final String error500 = "{\"" + keyException + "\":\"Something went wrong. Please try again later.\"}";

    /**
     * The bean used to manage user's authentication.
     */
    @EJB
    private Authentication authentication;
    
    /**
     * The bean used to manage the operations over Challenges.
     * This API class, in fact, expose over the Web some features
     * of the TrackManagement bean.
     */
    @EJB
    private ChallengeManagement challengeManagement;
    
    /**
     * The bean used to manage the operations over Tracks.
     * In this class is used to guarantee the consistency
     * of the Tracks added to the challenges.
     */
    @EJB
    private TrackManagement trackManagement;

//    @GET
//    @Produces("application/json")
//    @Path("/near/{latitude}/{longitude}/{distance}")
//    public String getNearChallenges(@PathParam(value = "latitude") String latitude, @PathParam(value = "longitude") String longitude, @PathParam(value = "distance") String distance, @QueryParam(value = "token") String token) {
//        Map<String, Object> challenges = new HashMap<String, Object>();
//        try {
//            authentication.tokenExists(token);
//            List<Challenge> challengesSet = challengeManagement.getNearChallenges(latitude, longitude, distance);
//            List<Map> challengesList = new LinkedList<Map>();
//            for (Challenge challenge : challengesSet) {
//                challengesList.add(challenge.toMap());
//            }
//            challenges.put("challenges", challengesList);
//        } catch (Exception ex) {
//            Logger.getLogger(UserResource.class.getName()).log(Level.SEVERE, null, ex);
//            challenges.put(keyException, ex.getMessage());
//        }
//        return JsonUtil.toJson(challenges);
//    }
    
    /**
     * Creates a new Challenge with the specified settings.
     * 
     * @param token the API call authentication token.
     * @param userId the Challenges creator's user id.
     * @param mainTrackId the Challenge's main Track id.
     * @param title the Challenge's title.
     * @param description the Challenge's description.
     * @param transportType DEPRECATED! The Challenge's transport type (the same of the main Track).
     *                      Now the Challenge's transport type is automatically inferred from the main Track!
     * @param lifeTime the Challenge's lifetime.
     * @param usersToShare the list of the users with whom share the Challenge.
     * @return If the request is valid, returns a JSON Response containing the
     *         created Challenge, otherwise a JSON error
     *         message as documented in the TrackMe WebService API Specifications document.
     */
    @POST
    @Produces("application/http")
    public Response createChallenge(@QueryParam(value = "token") String token,
            @FormParam("user_id") Long userId,
            @FormParam("main_track_id") Long mainTrackId,
            @FormParam("title") String title,
            @FormParam("description") String description,
            @FormParam("transport_type") String transportType,
            @FormParam("life_time") Long lifeTime,
            @FormParam("users_to_share") String usersToShare) {
        
        Map<String, String> response;
        try {
            
            Long authId = authentication.checkToken(token);
            if(authId >= 0 && authId == userId) {
                
                // TODO: [x] Controlli sul MainTrack userId - Challenge userId
                // TODO: [x] Controlli sul MainTrack TransportType - Challenge TransportType
                // TODO: [ ] Eliminare il campo TransportType
                // TODO: [ ] Implementare gli inviti!
                
                Track mainTrack = trackManagement.getCompleteTrack(mainTrackId.toString());
                if(mainTrack.getOwner().getId() == authId) {
                    
                    Challenge newChallenge = new Challenge();
                    newChallenge.setTitle(title);
                    newChallenge.setTransportType(mainTrack.getTransportType()); 
                    newChallenge.setDescription(description);
                    newChallenge.setLifeTime(lifeTime);
                    newChallenge.setOwner(new User(userId));
                    newChallenge.setMainTrack(mainTrack);
                    newChallenge = challengeManagement.createChallenge(newChallenge);
                    response = newChallenge.toMap();
                
                }
                else {
                    
                    return Response.status(Response.Status.UNAUTHORIZED)
                        .type(MediaType.APPLICATION_JSON)
                        .entity(invalidKey.replace(invalidKeyReplacement, "You are not the owner of the Main Track!")).build();
                
                }

            }
            
            else {
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            }
        
        } catch (Exception ex) {
            
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
        
    }

    /**
     * Adds the given Track to the specified Challenge.
     * 
     * @param challengeId the challenge Id.
     * @param token the API call authentication token.
     * @param trackId the id of the Track that must be added to the Challenge.
     * @return a JSON response containing a feedback about the operation result.
     */
    @PUT
    @Produces("application/http")
    @Path("/{id}/track")
    public Response addTrackToChallenge(@PathParam(value = "id") String challengeId,
            @QueryParam(value = "token") String token,
            @FormParam("track_id") Long trackId) {
        
        Map<String, String> response = new HashMap<String, String>();
        try {
            
            Track track = trackManagement.getCompleteTrack(trackId.toString());
            Long authId = authentication.checkToken(token);
            
            if(authId >= 0) {
                
                // TODO: Qui è possibile implementare il controllo per verificare
                //       se l'utente è fra gli invitati alla sfida
                if(track.getOwner().getId() == authId) {
            
                    challengeManagement.addTrackToChallenge(challengeId, track);
                    response.put("response", "ok");
                }
                else {
                    return Response.status(Response.Status.UNAUTHORIZED)
                        .type(MediaType.APPLICATION_JSON)
                        .entity(invalidKey.replace(invalidKeyReplacement, "You are not allowed to add this Track to the challenge!")).build();
                }
            }
            else {
                
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            
            }
            
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    }

    /**
     * Returns the requested Challenge with the list of all its Tracks.
     * 
     * @param challgengeId the requested Challenge's id.
     * @param token the API call authentication token.
     * @return If the request is valid, returns a JSON Response containing the
     *         requested Challenge within detailed information about its tracks, 
     *         otherwise a JSON error message as documented in the TrackMe 
     *         WebService API Specifications document.
     */
    @GET
    @Produces("application/http")
    @Path("/{id}")
    public Response getCompleteChallengeInfo(@PathParam(value = "id") String challgengeId,
            @QueryParam(value = "token") String token) {
        
        Map<String, Object> response;
        try {
            
            if(authentication.tokenExists(token)) {
                
                Challenge challenge = challengeManagement.getCompleteChallenge(challgengeId);
                response = challenge.toMap();
                
                logger.info("Traccia principale:" + challenge.getMainTrack().getId());
                logger.info("Numero di punti:" + challenge.getMainTrack().getPoints().size());
                
                response.put("main_points", challenge.getMainTrack().getPoints());
                response.put("user", challenge.getOwner().toMap());
                // TODO: Aggiungere gli invitati alla sfida
                
                List<Map> trackMaps = new LinkedList<Map>();
                for(Track track : challenge.getChallengesTracks()){
                    Map<String,Object> trackMap = track.toMap();
                    trackMap.put("points", track.getPoints());
                    trackMaps.add(trackMap);
                }       
                response.put("tracks", trackMaps);
            
            }
            else {
                
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            
            }
            
        } catch (TrackMeException ex) {
            
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        
        } catch (Exception ex) {
            
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }
    
    /**
     * Returns the list of all the challenges available for the logged in user.
     * 
     * @param token the API call authentication token.
     * @return If the request is valid, returns a JSON Response containing the
     *         requested Challenges, otherwise a JSON error message as documented 
     *         in the TrackMe WebService API Specifications document.
     */
    @GET
    @Produces("application/http")
    public Response getAllChallenges(@QueryParam(value = "token") String token) {
        Map<String, Object> response = new HashMap<String, Object>();
  
        try{
            
            // TODO: Resitutire solo quelle pubbliche e quelle in cui sei invitato
            if(authentication.tokenExists(token)) {
                
                List<Challenge> challenges = challengeManagement.getAllChallenge();
                List<Map> challengeMaps = new ArrayList<Map>(challenges.size());
                for(Challenge challenge : challenges){
                    Map<String,Object> challengeMap = challenge.toMap();
                    challengeMap.put("user_id", challenge.getOwner().getId());
                    challengeMap.put("main_track_id", null);  
                    if(challenge.getMainTrack() != null){
                        challengeMap.put("track_id", challenge.getMainTrack().getId());  
                    }
                    challengeMaps.add(challengeMap);
                }
                response.put("challenges", challengeMaps);
            
            }
            else {
                
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            
            }
            
        } catch (TrackMeException ex) {

            return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON)
                .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
           
    }

}