package org.unito.trackme.restful;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.beans.Authentication;
import org.unito.trackme.entities.Challenge;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.User;
import org.unito.trackme.exception.AuthenticationException;
import org.unito.trackme.exception.TrackMeException;
import org.unito.trackme.managements.UsersManagement;
import org.unito.trackme.util.JsonUtil;

/**
 * RESTful WebService exposing some actions over Users.
 * Further informations about methods and available actions
 * can be found in the TrackMe WebService API Specifications document.
 */
@Path("user")
@Stateless
public class UserResource {

    private static final Logger logger = LogManager.getLogger(Authentication.class);
    private static final String keyException = "error";
    private static final String invalidKey = "{\"" + keyException + "\":\"string\"}";
    private static final String invalidKeyReplacement = "string";
    private static final String error500 = "{\"" + keyException + "\":\"Something went wrong. Please try again later.\"}";
    
    /**
     * The bean used to manage user's authentication.
     */
    @EJB
    private Authentication authentication;
    
    /**
     * The bean used to manage the operations over Users.
     * This API class, in fact, expose over the Web some features
     * of the UserManagement bean.
     */
    @EJB
    private UsersManagement usersManagement;

    /**
     * Authenticates an API user.
     * 
     * @param username the user's username.
     * @param password the user's password.
     * @return a JSON Response with some user's information, as described in the 
     *         TrackMe WebService API Specifications document, 
     *         and the API call's authentication token.
     */
    @POST
    @Produces("application/http")
    @Path("login")
    public Response login(@FormParam("username") String username, @FormParam("password") String password) {
        
        logger.info("API login parametri: {Username: " + username + ", password: " + password + "}");
        
        Map<String, String> response;
        try {
            
            User user = authentication.login(username, password);
            response = user.toMap();                    // le info sull'utente
            response.put("token", user.getToken());     // il token di autenticazione per le chiamate API
        
        } catch (AuthenticationException ex) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }

        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }
    
    /**
     * Returns the profile of the specified user.
     * 
     * @param userId the ID of the requested user.
     * @param token the authentication token.
     * @return If the request is valid, returns a JSON Response containing the public
     *         information about the requested user, otherwise a JSON error
     *         message as documented in the TrackMe WebService API Specifications document.       
     */
    @GET
    @Produces("application/http")
    @Path("{id}")
    public Response getUserInfo(@PathParam(value = "id") Long userId, @QueryParam(value = "token") String token) {
        
        logger.info("API Get User Info: {user-id: " + userId + ", token: " + token + "}");
        
        try {

            if(authentication.tokenExists(token)) {
                User user = usersManagement.getUser(userId.toString());
                return Response.ok(JsonUtil.toJson(user.toMap()), MediaType.APPLICATION_JSON).build();
            }
            else {
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            }
     
        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
    }

    /**
     * Returns the list of the tracks recorded by the specified user.
     * 
     * @param userId the ID of the User.
     * @param token the authentication token.
     * @return If the request is valid, returns a JSON Response containing the list
     *         of the user's tracks, otherwise a JSON error
     *         message as documented in the TrackMe WebService API Specifications document.    
     */
    @GET
    @Produces("application/http")
    @Path("{id}/tracks")
    public Response getTracks(@PathParam(value = "id") Long userId, @QueryParam(value = "token") String token) {
        
        Map<String, Object> response = new HashMap<String, Object>();
        try {
            
            if(authentication.tokenExists(token)) {
               
                List<Track> userTracks = usersManagement.getTracks(userId.toString());
                
                List<Map> mapUserTraks = new LinkedList<Map>();
                for (Track track : userTracks) {
                    mapUserTraks.add(track.toMap());
                }
                response.put("tracks", mapUserTraks);
            
            }
            else {
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            }            
        
        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }

    /**
     * Return the list of the Challenges created or joined by the specified User.
     * 
     * @param userId the ID of the specified User.
     * @param token the authentication token.
     * @return If the request is valid, returns a JSON Response containing the list
     *         of the user's challenges, otherwise a JSON error
     *         message as documented in the TrackMe WebService API Specifications document.  
     */
    @GET
    @Produces("application/http")
    @Path("{id}/challenges")
    public Response getChallenges(@PathParam(value = "id") Long userId, @QueryParam(value = "token") String token) {
        
        Map<String, Object> response = new HashMap<String, Object>();
        
        try {
            
            if(authentication.tokenExists(token)) {
                
                List<Challenge> userChallenges = usersManagement.getChallenges(userId.toString());
                
                List<Map> mapUserChallenges = new LinkedList<Map>();
                for (Challenge challenge : userChallenges) {
                    mapUserChallenges.add(challenge.toMap());
                }
                
                response.put("challenges", mapUserChallenges);
            }
            else {
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            }
            
        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }

    /**
     * Creates a new User.
     * 
     * This API call is allowed only by client that uses
     * a special ApplicationToken.
     * 
     * @param username the new User username.
     * @param password the new User password.
     * @param email the new User email.
     * @return a feedback about the success of the operation.
     */
    @POST
    @Produces("application/http")
    public Response createUser(@FormParam("username") String username, @FormParam("password") String password, @FormParam("email") String email) {
          
        // TODO: Permettere che questa chiamata venga autorizzata solo
        // da client speciali attraverso un ApplicationToken
          
        Map<String, String> response = new HashMap<String, String>();
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);
        newUser.setEmail(email);
        
        try {
            usersManagement.createUser(newUser);
            response.put("response", "ok");
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }
    
    // TODO: Update User

}
