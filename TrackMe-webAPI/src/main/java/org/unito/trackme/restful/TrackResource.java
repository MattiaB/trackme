package org.unito.trackme.restful;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.beans.Authentication;
import org.unito.trackme.entities.Point;
import org.unito.trackme.entities.Track;
import org.unito.trackme.exception.TrackMeException;
import org.unito.trackme.managements.TrackManagement;
import org.unito.trackme.util.JsonUtil;

/**
 * RESTful WebService exposing some actions over Tracks.
 * Further informations about methods and available actions
 * can be found in the TrackMe WebService API Specifications document.
 */
@Path("track")
@Stateless
public class TrackResource {

    private static final Logger logger = LogManager.getLogger(TrackResource.class);
    private static final String keyException = "error";
    private static final String invalidKey = "{\"" + keyException + "\":\"string\"}";
    private static final String invalidKeyReplacement = "string";
    private static final String error500 = "{\"" + keyException + "\":\"Something went wrong. Please try again later.\"}";
    
    /**
     * The bean used to manage user's authentication.
     */
    @EJB
    private Authentication authentication;
    
    /**
     * The bean used to manage the operations over Tracks.
     * This API class, in fact, expose over the Web some features
     * of the TrackManagement bean.
     */
    @EJB
    private TrackManagement trackManagement;

    /**
     * Returns the requested Track with all its location points.
     * 
     * @param trackId the requested track's id.
     * @param token the API call authentication token.
     * @return If the request is valid, returns a JSON Response containing all
     *         the information about the requested track, otherwise a JSON error
     *         message as documented in the TrackMe WebService API Specifications document.
     */
    @GET
    @Produces("application/http")
    @Path("{id}")
    public Response getCompleteTrackInfo(@PathParam(value = "id") Long trackId, @QueryParam(value = "token") String token) {
        
        Map<String, Object> response;
        try {
           
            if(authentication.tokenExists(token)) {
                
                Track track = trackManagement.getCompleteTrack(trackId.toString());
                response = track.toMap();
                response.put("points", track.getPoints());
            
            }
            else {  
                
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            
            }
            
        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }

    /**
     * Return the list of the locations of the requested Track.
     * 
     * @param trackId the requested track's id.
     * @param token the API call authentication token.
     * @return If the request is valid, returns a JSON Response containing the
     *         list of the locations of the requested track, otherwise a JSON error
     *         message as documented in the TrackMe WebService API Specifications document.
     */
    @GET
    @Produces("application/http")
    @Path("/{id}/points")
    public Response getTrackPoints(@PathParam(value = "id") Long trackId, @QueryParam(value = "token") String token) {
        
        Map<String, Object> response = new HashMap<String, Object>();
        try {
            
            if(authentication.tokenExists(token)) {
                
                List<Point> points = trackManagement.getPoints(trackId.toString());
                response.put("id", trackId);
                response.put("points", points);
            
            }
            else {
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            }
            
        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }

    /**
     * Add the given location points to the specified Track.
     * Notice that in order to successfully add the points, 
     * the Track must be "not ended" as specified 
     * in the TrackMe WebService API Specifications document.
     * 
     * @param trackId the if of the track where the points has to be added.
     * @param jsonPoints the JSON object containing the points must be added to the Track.
     * @param token the API call authentication token.
     * @return a JSON response containing a feedback about the operation result.
     */
    @PUT
    @Produces("application/http")
    @Path("{id}/points")
    public Response putPointsInTrack(@PathParam(value = "id") Long trackId,
            @QueryParam(value = "token") String token, @FormParam("points") String jsonPoints) {
        
        Map<String, String> response = new HashMap<String, String>();
        try {
            
            Long authId = authentication.checkToken(token);
            if(authId  >= 0) {
                
                Track track = trackManagement.getCompleteTrack(trackId.toString());
                if(track.getOwner().getId() == authId) {    // Se la traccia è dell'utente autenticato
                
                    if (jsonPoints != null && !jsonPoints.trim().equals("")) {

                        logger.info("Punti json:" + jsonPoints);

                        List<Map> points = JsonUtil.fromJson(jsonPoints, List.class);   
                        if (!points.isEmpty()) {

                            List<Point> pointList = new LinkedList<Point>();
                            for (Map point : points) {
                                pointList.add(new Point((String) point.get("latitude"), (String) point.get("longitude"), (String) point.get("elevation"), (String) point.get("time")));
                            }

                            trackManagement.putPoints(trackId.toString(), pointList);
                            response.put("response", "ok");

                        } else {

                            logger.info("Non ci sono punti nel json");
                            response.put("response", "No Points");

                        }

                    } else {
                        response.put("response", "Empty Message");
                    }
                    
                }
                else {
                    return Response.status(Response.Status.UNAUTHORIZED)
                        .type(MediaType.APPLICATION_JSON)
                        .entity(invalidKey.replace(invalidKeyReplacement, "You are not the owner of the Track!")).build();
                }
            }
            else {
                
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            
            }
            
        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
       
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }

    /**
     * Marks the specified Track as ended.
     * As described in the TrackMe WebService API Specifications document,
     * once a Track is ended, nomore points can be added to.
     * 
     * @param trackId the id of the track that must be ended.
     * @param token the API call authentication token.
     * @return a JSON response containing a feedback about the operation result.
     */
    @PUT
    @Produces("application/http")
    @Path("{id}/end")
    public Response endTrack(@PathParam(value = "id") Long trackId, @QueryParam(value = "token") String token) {
        
        Map<String, String> response = new HashMap<String, String>();
        try {
            
            Long authId = authentication.checkToken(token);
            if(authId  >= 0) {
                
                Track track = trackManagement.getCompleteTrack(trackId.toString());
                if(track.getOwner().getId() == authId) {    // Se la traccia è dell'utente autenticato
                    trackManagement.endRealTimeTrack(trackId.toString());
                    response.put("response", "ok");
                }
                else {
                    return Response.status(Response.Status.UNAUTHORIZED)
                        .type(MediaType.APPLICATION_JSON)
                        .entity(invalidKey.replace(invalidKeyReplacement, "You are not the owner of the Track!")).build();
                }
            
            }
            else { 
                
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            
            }

        } catch (TrackMeException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, ex.getMessage())).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }

    /**
     * Creates a new Track with the given settings.
     * 
     * @param token the API call authentication token.
     * @param userId the Track owner's user id.
     * @param title the Track's title.
     * @param transportType the Track's transport type.
     * @return If the request is valid, returns a JSON Response containing the
     *         created Track, otherwise a JSON error
     *         message as documented in the TrackMe WebService API Specifications document.
     */
    @POST
    @Produces("application/http")
    public Response newTrack(@QueryParam(value = "token") String token,
            @FormParam("user_id") Long userId,
            @FormParam("title") String title,
            @FormParam("transport_type") String transportType) {
       
        Map<String, String> response;
        
        try {
            
            Long authId = authentication.checkToken(token);
            if(authId  >= 0 && authId == userId) {
                
                Track newTrack = new Track();
                newTrack.setTitle(title);
                newTrack.setTransportType(transportType);
                newTrack = trackManagement.createTrack(newTrack, userId);
                response = newTrack.toMap();
            
            }
            else {
                
                return Response.status(Response.Status.UNAUTHORIZED)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(invalidKey.replace(invalidKeyReplacement, "You must login first!")).build();
            
            }
            
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(error500).build();
        }
        
        return Response.ok(JsonUtil.toJson(response), MediaType.APPLICATION_JSON).build();
    
    }

}
