require 'nokogiri'
require 'rest_client'
require 'json'
require_relative 'trackme_model'
require_relative 'json_rest_client'

filename = "Via Bogino, 8B.gpx"
def parse_gpx(filename)
    file = File.open(filename)
    doc = Nokogiri::XML(file)
    trackpoints = doc.xpath('//xmlns:trkpt')
    route = Array.new
    trackpoints.each do |trkpt|
      lat = trkpt.xpath('@lat').to_s
      lon = trkpt.xpath('@lon').to_s
	  trkptChildren = trkpt.children;
      ele = trkptChildren.find{|n| n.name == 'ele'}.text.strip.to_s
	  time = trkptChildren.find{|n| n.name == 'time'}.text.strip.to_s
      route << {latitude: lat, longitude: lon, elevation: ele, time: time}
    end
    route
end
route = parse_gpx(filename)

JsonRestClient.begin_url = 'http://localhost:8080/api'

user = User.new JsonRestClient.post('/user/login',
                                              :username => User.get_test_user.username,
                                              :password => User.get_test_user.password)[0]
puts user
new_track, response = JsonRestClient.post('/track?token='+user.token,
	                                          :user_id => user.id, :title => "Track test",
                                             :transport_type => "transport test")
puts new_track
track_id = new_track['id']

route.each do |point|
	puts point.to_json 
	response_body, response = JsonRestClient.put("/track/#{track_id}/points?token="+user.token,:points=> [point].to_json)
	puts response_body	
	sleep(1)
end
