require 'rest_client'
require 'json'
module JsonRestClient
  @@url

  def self.begin_url=(url)
    @@url = url
  end

  ['get', 'delete', 'head'].each do |method_name|
    module_eval "
    def self.#{method_name}(url, headers={}, &block)
      url = @@url + url
      response = RestClient.#{method_name}(url, headers, &block)
      [json_parse(response), response]
    end
  "
  end

  ['post', 'patch', 'put'].each do |method_name|
    module_eval "
    def self.#{method_name}(url, payload, headers={}, &block)
      url = @@url + url
      response = RestClient.#{method_name}(url, payload, headers, &block)
      [json_parse(response), response]
    end
  "
  end
  private
  def self.json_parse(string)
    JSON.parse string
  end
end