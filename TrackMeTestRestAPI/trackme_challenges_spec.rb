require 'rspec'
require 'json'
require_relative 'trackme_model'
require_relative 'json_rest_client'

JsonRestClient.begin_url = 'http://localhost:8080/api'

describe "/challenge" do
  let(:points) { '[{"latitude":"45.000091","longitude":"7.680159","elevation":"267.7","time":"2013-03-08T17:34:58.000Z"},{"latitude":"45.000063","longitude":"7.680077","elevation":"270.2","time":"2013-03-08T17:34:59.000Z"},{"latitude":"45.000038","longitude":"7.680021","elevation":"270.1","time":"2013-03-08T17:35:00.000Z"},{"latitude":"44.999997","longitude":"7.679958","elevation":"269.8","time":"2013-03-08T17:35:01.000Z"},{"latitude":"44.99998","longitude":"7.679904","elevation":"269.3","time":"2013-03-08T17:35:02.000Z"},{"latitude":"44.999975","longitude":"7.679847","elevation":"270.7","time":"2013-03-08T17:35:03.000Z"},{"latitude":"44.999968","longitude":"7.679787","elevation":"273.4","time":"2013-03-08T17:35:05.000Z"},{"latitude":"44.999973","longitude":"7.679712","elevation":"275.6","time":"2013-03-08T17:35:06.000Z"},{"latitude":"44.999996","longitude":"7.679645","elevation":"278.7","time":"2013-03-08T17:35:07.000Z"},{"latitude":"45.000012","longitude":"7.679584","elevation":"282.5","time":"2013-03-08T17:35:08.000Z"},{"latitude":"45.000027","longitude":"7.679538","elevation":"284.3","time":"2013-03-08T17:35:09.000Z"},{"latitude":"45.000036","longitude":"7.679501","elevation":"285.9","time":"2013-03-08T17:35:10.000Z"},{"latitude":"45.000048","longitude":"7.679453","elevation":"279.3","time":"2013-03-08T17:35:24.000Z"},{"latitude":"45.00005","longitude":"7.679419","elevation":"279.3","time":"2013-03-08T17:35:25.000Z"},{"latitude":"45.000089","longitude":"7.679416","elevation":"281.5","time":"2013-03-08T17:35:27.000Z"},{"latitude":"45.000095","longitude":"7.679416","elevation":"281","time":"2013-03-08T17:35:28.000Z"},{"latitude":"45.000125","longitude":"7.679376","elevation":"280.9","time":"2013-03-08T17:35:32.000Z"},{"latitude":"45.000124","longitude":"7.679348","elevation":"280.9","time":"2013-03-08T17:35:33.000Z"},{"latitude":"45.000126","longitude":"7.679309","elevation":"281.1","time":"2013-03-08T17:35:34.000Z"},{"latitude":"45.000122","longitude":"7.679256","elevation":"281","time":"2013-03-08T17:35:35.000Z"},{"latitude":"45.000124","longitude":"7.679204","elevation":"280.9","time":"2013-03-08T17:35:36.000Z"},{"latitude":"45.000132","longitude":"7.679142","elevation":"281.1","time":"2013-03-08T17:35:37.000Z"},{"latitude":"45.000129","longitude":"7.679066","elevation":"280.6","time":"2013-03-08T17:35:38.000Z"},{"latitude":"45.000119","longitude":"7.678955","elevation":"278.1","time":"2013-03-08T17:35:39.000Z"},{"latitude":"45.000106","longitude":"7.678863","elevation":"276.5","time":"2013-03-08T17:35:40.000Z"},{"latitude":"45.000079","longitude":"7.678788","elevation":"274.8","time":"2013-03-08T17:35:41.000Z"},{"latitude":"45.000046","longitude":"7.678723","elevation":"274.4","time":"2013-03-08T17:35:42.000Z"},{"latitude":"45.00001","longitude":"7.678659","elevation":"273.2","time":"2013-03-08T17:35:43.000Z"},{"latitude":"44.999958","longitude":"7.67858","elevation":"274.2","time":"2013-03-08T17:35:44.000Z"},{"latitude":"44.999909","longitude":"7.678507","elevation":"275.1","time":"2013-03-08T17:35:45.000Z"},{"latitude":"44.999858","longitude":"7.678436","elevation":"275.2","time":"2013-03-08T17:35:46.000Z"},{"latitude":"44.999814","longitude":"7.678376","elevation":"275.7","time":"2013-03-08T17:35:47.000Z"},{"latitude":"44.999782","longitude":"7.678326","elevation":"276.4","time":"2013-03-08T17:35:48.000Z"},{"latitude":"44.999742","longitude":"7.678295","elevation":"275.5","time":"2013-03-08T17:35:49.000Z"},{"latitude":"44.999671","longitude":"7.678255","elevation":"271.7","time":"2013-03-08T17:35:50.000Z"},{"latitude":"44.999618","longitude":"7.678211","elevation":"271","time":"2013-03-08T17:35:51.000Z"},{"latitude":"44.999584","longitude":"7.678199","elevation":"270.5","time":"2013-03-08T17:35:52.000Z"},{"latitude":"44.999557","longitude":"7.67819","elevation":"270.3","time":"2013-03-08T17:35:53.000Z"},{"latitude":"44.999516","longitude":"7.678189","elevation":"269.3","time":"2013-03-08T17:35:55.000Z"},{"latitude":"44.999502","longitude":"7.678188","elevation":"269.7","time":"2013-03-08T17:35:56.000Z"},{"latitude":"44.999478","longitude":"7.678151","elevation":"277.3","time":"2013-03-08T17:36:13.000Z"},{"latitude":"44.999467","longitude":"7.678137","elevation":"278.6","time":"2013-03-08T17:36:14.000Z"},{"latitude":"44.999435","longitude":"7.678133","elevation":"280.7","time":"2013-03-08T17:36:16.000Z"},{"latitude":"44.999414","longitude":"7.678142","elevation":"280.4","time":"2013-03-08T17:36:17.000Z"},{"latitude":"44.999391","longitude":"7.678155","elevation":"280.3","time":"2013-03-08T17:36:18.000Z"},{"latitude":"44.999366","longitude":"7.678178","elevation":"280.2","time":"2013-03-08T17:36:19.000Z"},{"latitude":"44.999336","longitude":"7.678208","elevation":"280.3","time":"2013-03-08T17:36:20.000Z"},{"latitude":"44.999303","longitude":"7.67825","elevation":"280.1","time":"2013-03-08T17:36:21.000Z"},{"latitude":"44.999264","longitude":"7.678298","elevation":"280","time":"2013-03-08T17:36:22.000Z"},{"latitude":"44.99922","longitude":"7.67835","elevation":"279.8","time":"2013-03-08T17:36:23.000Z"},{"latitude":"44.999169","longitude":"7.678407","elevation":"279.5","time":"2013-03-08T17:36:24.000Z"},{"latitude":"44.999115","longitude":"7.678475","elevation":"279.2","time":"2013-03-08T17:36:25.000Z"},{"latitude":"44.999062","longitude":"7.678542","elevation":"279","time":"2013-03-08T17:36:26.000Z"},{"latitude":"44.999016","longitude":"7.67861","elevation":"278.9","time":"2013-03-08T17:36:27.000Z"},{"latitude":"44.998973","longitude":"7.678675","elevation":"278.8","time":"2013-03-08T17:36:28.000Z"},{"latitude":"44.998935","longitude":"7.678737","elevation":"278.7","time":"2013-03-08T17:36:29.000Z"},{"latitude":"44.998895","longitude":"7.678797","elevation":"278.6","time":"2013-03-08T17:36:30.000Z"},{"latitude":"44.998854","longitude":"7.678849","elevation":"278.4","time":"2013-03-08T17:36:31.000Z"},{"latitude":"44.99882","longitude":"7.678899","elevation":"278.4","time":"2013-03-08T17:36:32.000Z"},{"latitude":"44.998784","longitude":"7.678944","elevation":"278.3","time":"2013-03-08T17:36:33.000Z"},{"latitude":"44.998747","longitude":"7.678989","elevation":"278.1","time":"2013-03-08T17:36:34.000Z"},{"latitude":"44.998706","longitude":"7.67904","elevation":"278.2","time":"2013-03-08T17:36:35.000Z"},{"latitude":"44.998657","longitude":"7.679098","elevation":"278.2","time":"2013-03-08T17:36:36.000Z"},{"latitude":"44.998609","longitude":"7.67916","elevation":"278.1","time":"2013-03-08T17:36:37.000Z"},{"latitude":"44.998567","longitude":"7.679225","elevation":"278.2","time":"2013-03-08T17:36:38.000Z"},{"latitude":"44.998526","longitude":"7.679291","elevation":"278.3","time":"2013-03-08T17:36:39.000Z"},{"latitude":"44.998491","longitude":"7.679357","elevation":"278.4","time":"2013-03-08T17:36:40.000Z"},{"latitude":"44.998452","longitude":"7.679417","elevation":"278.4","time":"2013-03-08T17:36:41.000Z"},{"latitude":"44.998414","longitude":"7.679467","elevation":"278.4","time":"2013-03-08T17:36:42.000Z"},{"latitude":"44.998374","longitude":"7.679513","elevation":"278.4","time":"2013-03-08T17:36:43.000Z"},{"latitude":"44.998339","longitude":"7.679558","elevation":"278.4","time":"2013-03-08T17:36:44.000Z"},{"latitude":"44.998308","longitude":"7.679606","elevation":"278.5","time":"2013-03-08T17:36:45.000Z"},{"latitude":"44.99828","longitude":"7.679647","elevation":"278.4","time":"2013-03-08T17:36:46.000Z"},{"latitude":"44.99826","longitude":"7.679693","elevation":"278.5","time":"2013-03-08T17:36:47.000Z"},{"latitude":"44.998234","longitude":"7.679735","elevation":"278.6","time":"2013-03-08T17:36:48.000Z"},{"latitude":"44.998207","longitude":"7.679779","elevation":"278.7","time":"2013-03-08T17:36:49.000Z"},{"latitude":"44.998173","longitude":"7.679814","elevation":"278.6","time":"2013-03-08T17:36:50.000Z"},{"latitude":"44.998143","longitude":"7.679849","elevation":"278.4","time":"2013-03-08T17:36:51.000Z"},{"latitude":"44.998114","longitude":"7.679887","elevation":"278.4","time":"2013-03-08T17:36:52.000Z"},{"latitude":"44.998085","longitude":"7.679928","elevation":"278.5","time":"2013-03-08T17:36:53.000Z"},{"latitude":"44.998058","longitude":"7.679973","elevation":"278.6","time":"2013-03-08T17:36:54.000Z"},{"latitude":"44.998031","longitude":"7.680018","elevation":"278.7","time":"2013-03-08T17:36:55.000Z"},{"latitude":"44.998016","longitude":"7.680065","elevation":"278.8","time":"2013-03-08T17:36:56.000Z"},{"latitude":"44.998008","longitude":"7.68011","elevation":"278.6","time":"2013-03-08T17:36:57.000Z"},{"latitude":"44.998003","longitude":"7.680152","elevation":"278.5","time":"2013-03-08T17:36:58.000Z"},{"latitude":"44.99801","longitude":"7.680192","elevation":"278.5","time":"2013-03-08T17:36:59.000Z"},{"latitude":"44.998024","longitude":"7.680236","elevation":"278.5","time":"2013-03-08T17:37:00.000Z"},{"latitude":"44.998049","longitude":"7.680285","elevation":"278.3","time":"2013-03-08T17:37:01.000Z"},{"latitude":"44.998074","longitude":"7.680326","elevation":"278.3","time":"2013-03-08T17:37:02.000Z"},{"latitude":"44.998098","longitude":"7.680368","elevation":"278.1","time":"2013-03-08T17:37:03.000Z"},{"latitude":"44.99812","longitude":"7.680403","elevation":"278.2","time":"2013-03-08T17:37:04.000Z"},{"latitude":"44.998143","longitude":"7.680446","elevation":"278.2","time":"2013-03-08T17:37:05.000Z"},{"latitude":"44.998168","longitude":"7.68048","elevation":"277.8","time":"2013-03-08T17:37:06.000Z"},{"latitude":"44.998187","longitude":"7.680526","elevation":"277.6","time":"2013-03-08T17:37:07.000Z"},{"latitude":"44.998201","longitude":"7.680585","elevation":"277.4","time":"2013-03-08T17:37:08.000Z"},{"latitude":"44.998203","longitude":"7.680657","elevation":"277.3","time":"2013-03-08T17:37:09.000Z"},{"latitude":"44.998199","longitude":"7.680735","elevation":"277.1","time":"2013-03-08T17:37:10.000Z"},{"latitude":"44.998196","longitude":"7.680816","elevation":"277.1","time":"2013-03-08T17:37:11.000Z"},{"latitude":"44.998196","longitude":"7.680903","elevation":"277.2","time":"2013-03-08T17:37:12.000Z"},{"latitude":"44.998191","longitude":"7.681","elevation":"277.2","time":"2013-03-08T17:37:13.000Z"},{"latitude":"44.998186","longitude":"7.681105","elevation":"277.4","time":"2013-03-08T17:37:14.000Z"},{"latitude":"44.998178","longitude":"7.681214","elevation":"277.4","time":"2013-03-08T17:37:15.000Z"},{"latitude":"44.998177","longitude":"7.681331","elevation":"277.2","time":"2013-03-08T17:37:16.000Z"},{"latitude":"44.998171","longitude":"7.681457","elevation":"276.7","time":"2013-03-08T17:37:17.000Z"},{"latitude":"44.998162","longitude":"7.681585","elevation":"276.3","time":"2013-03-08T17:37:18.000Z"},{"latitude":"44.998144","longitude":"7.6817","elevation":"276","time":"2013-03-08T17:37:19.000Z"},{"latitude":"44.998134","longitude":"7.681828","elevation":"276.1","time":"2013-03-08T17:37:20.000Z"},{"latitude":"44.998124","longitude":"7.681965","elevation":"276.5","time":"2013-03-08T17:37:21.000Z"},{"latitude":"44.998108","longitude":"7.682125","elevation":"276.6","time":"2013-03-08T17:37:22.000Z"},{"latitude":"44.998091","longitude":"7.682268","elevation":"276.4","time":"2013-03-08T17:37:23.000Z"},{"latitude":"44.998076","longitude":"7.682403","elevation":"276.4","time":"2013-03-08T17:37:24.000Z"},{"latitude":"44.998062","longitude":"7.682533","elevation":"276.3","time":"2013-03-08T17:37:25.000Z"},{"latitude":"44.998045","longitude":"7.682667","elevation":"276.2","time":"2013-03-08T17:37:26.000Z"},{"latitude":"44.998029","longitude":"7.682793","elevation":"276","time":"2013-03-08T17:37:27.000Z"},{"latitude":"44.998026","longitude":"7.682908","elevation":"276.3","time":"2013-03-08T17:37:28.000Z"},{"latitude":"44.998018","longitude":"7.683024","elevation":"276.4","time":"2013-03-08T17:37:29.000Z"},{"latitude":"44.998004","longitude":"7.683125","elevation":"276.4","time":"2013-03-08T17:37:30.000Z"},{"latitude":"44.998007","longitude":"7.683233","elevation":"275.3","time":"2013-03-08T17:37:31.000Z"},{"latitude":"44.998014","longitude":"7.683342","elevation":"274.5","time":"2013-03-08T17:37:32.000Z"}]'
  }
  let(:track_titles){['track1','track2','track3','track4']}
  before(:all) do
    user = User.new JsonRestClient.post('/user/login',
                                              :username => User.get_test_user.username,
                                              :password => User.get_test_user.password)[0]
    begin
      response_body, response = JsonRestClient.post('/user',
                                                    :email => user.email,
                                                    :username => user.username, :password => user.password)
      response.code.should == 200
      user.set_from_hash(response_body)
    rescue => e
      e.response.code.should == 500
    end

    def newTrack(title,user)
      new_track, response = JsonRestClient.post('/track?token='+user.token,
                                                :user_id => user.id, :title => title,
                                                :transport_type => "transport test")

      track_id = new_track['id']

      response_body, response = JsonRestClient.put("/track/#{track_id}/points?token="+user.token, :points => points)
      response_body, response = JsonRestClient.put("/track/#{track_id}/end?token="+user.token,nil)
    end
    user_tracks, response = JsonRestClient.get("/user/#{user.id}/tracks?token="+user.token)
    unless(user_tracks['tracks'].count > track_titles.count) then
      track_titles.each do |track|
        newTrack(track,user)
      end
    end
  end

  context 'simply challenge story' do
    let(:user) { User.new JsonRestClient.post('/user/login',
                                              :username => User.get_test_user.username,
                                              :password => User.get_test_user.password)[0] }
    it 'should create a new challenge' do
      user_tracks, response = JsonRestClient.get("/user/#{user.id}/tracks?token="+user.token)

      response.code.should == 200
      user_tracks.should_not nil
      user_tracks['tracks'].should_not nil
      user_tracks['tracks'].count.should > 4
      total_tracks = user_tracks['tracks'].count

      main_track = user_tracks['tracks'][total_tracks-1]

      new_challenge, response = JsonRestClient.post('/challenge?token='+user.token,
                                                    :user_id => user.id, :title => 'Challenge1',
                                                    :transport_type => "transport test",
                                                    :description => 'description', :life_time => '3600',
                                                    :users_to_share => '[]', :main_track_id => main_track['id'])
      response.code.should == 200
      new_challenge.should_not nil
      new_challenge['id'].should_not nil

      def add_track_to_challenge(track, challenge)
        response_body, response = JsonRestClient.put("/challenge/#{challenge['id']}/track?token="+user.token,
                                                     :track_id => track['id'])
        response.code.should == 200
        response_body.should_not nil
        response_body['response'].should == 'ok'
      end

      second_track = user_tracks['tracks'][total_tracks-2]
      add_track_to_challenge(second_track, new_challenge)

      third_track = user_tracks['tracks'][total_tracks-3]
      add_track_to_challenge(third_track, new_challenge)

      fourth_track = user_tracks['tracks'][total_tracks-4]
      add_track_to_challenge(fourth_track, new_challenge)

      complete_challenge, response = JsonRestClient.get("/challenge/#{new_challenge['id']}?token="+user.token)
      response.code.should == 200
      complete_challenge.should_not nil
      complete_challenge['id'].should == new_challenge['id']
      #complete_challenge['main_points'].should == JSON.parse(points)
      complete_challenge['user']['id'].should == user.id
      complete_challenge['tracks'].count.should == track_titles.count-1

      complete_challenge, response = JsonRestClient.get("/challenge/#{new_challenge['id']}?token="+user.token)

      response_body, response = JsonRestClient.get("/user/#{user.id}/challenges?token="+user.token)
      response_body['challenges'].count.should > 0

      challenges = JsonRestClient.get("/challenge/?token="+user.token)
      challenges.count.should > 1
      challenge = challenges[0]
      challenge['user_id'].should_not nil
      challenge['main_track_id'].should_not nil


    end
  end
end
