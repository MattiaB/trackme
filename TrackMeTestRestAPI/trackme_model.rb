class User
  attr_accessor :id, :username, :email, :password, :token, :tracks

  def initialize(fields = nil)
    set_from_hash fields unless fields == nil
  end

  def set_from_hash(fields)
    @id = fields['id']
    @username = fields['username']
    @email = fields['email']
    @password = fields['password']
    @token = fields['token']
    @tracks = fields['tracks']
  end

  def self.get_test_user
    User.new({'email'=>'test@test.it','username'=>'test','password'=>'test'})
  end

  def to_s
    "id:#{id},username:#{username},email:#{email}"
  end
end