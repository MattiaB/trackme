require'rspec'
require_relative 'trackme_model'
require_relative 'json_rest_client'

JsonRestClient.begin_url = 'http://localhost:8080/api'

describe '/user' do
  subject { user }


  context 'create a test user' do
    let(:user) { User.get_test_user }

    it 'should do create user' do
      begin
        response_body, response = JsonRestClient.post('/user',
                                                      :email => user.email,
                                                      :username => user.username, :password => user.password)
        response.code.should == 200
      rescue => e
        e.response.code.should == 500
      end
    end
    it 'should do login' do
      response_body, response = JsonRestClient.post('/user/login',
                                                    :username => user.username, :password => user.password)
      response.code.should == 200
      response_body.should_not nil
      response_body['username'].should == 'test'
      response_body['token'].should_not nil
      user.set_from_hash response_body
    end
  end
  context 'get user info' do
    let(:user) { User.new JsonRestClient.post('/user/login',
                                              :username => User.get_test_user.username,
                                              :password => User.get_test_user.password)[0] }

    it 'should get user info' do
      response_body, response = JsonRestClient.get("/user/#{user.id}", {:params => {:token => user.token}})
      response.code.should == 200
      response_body.should_not nil
      response_body['username'].should == 'test'
      response_body['email'].should == 'test@test.it'
    end
  end
end