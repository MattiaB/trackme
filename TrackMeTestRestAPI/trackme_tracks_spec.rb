require 'rspec'
require 'json'
require_relative 'trackme_model'
require_relative 'json_rest_client'

JsonRestClient.begin_url = 'http://localhost:8080/api'

describe "/tracks" do

  context 'simply track story' do
    let(:user) { User.new JsonRestClient.post('/user/login',
                                              :username => User.get_test_user.username,
                                              :password => User.get_test_user.password)[0] }
    it 'should create a new track' do
      new_track, response = JsonRestClient.post('/track?token='+user.token,
                                                    :user_id => user.id, :title => "Track test",
                                                    :transport_type => "transport test")
      response.code.should == 200
      new_track.should_not nil
      new_track['id'].should_not nil
      track_id = new_track['id']

      points = '[{"latitude": "32.32312", "longitude": "43.23331", "elevation": "323", "time": "time"},
                {"latitude": "32.32312", "longitude": "43.23331", "elevation": "323", "time": "time"}]'
      response_body, response = JsonRestClient.put("/track/#{track_id}/points?token="+user.token,:points=> points)
      response.code.should == 200
      response_body.should_not nil
      response_body['response'].should == 'ok'

      response_body, response = JsonRestClient.get("/track/#{track_id}/points?token="+user.token)
      response_body['id'].should == track_id
      response_body['points'].should == JSON.parse(points)

      response_body, response = JsonRestClient.get("/track/#{track_id}?token="+user.token)
      response_body['id'].should == track_id
      response_body['title'].should == new_track['title']
      response_body['transport_type'].should == new_track['transport_type']
      response_body['real_time'].should == true
      response_body['points'].should == JSON.parse(points)

      response_body, response = JsonRestClient.put("/track/#{track_id}/end?token="+user.token,{})
      response.code.should == 200
      response_body.should_not nil
      response_body['response'].should == 'ok'

      response_body, response = JsonRestClient.get("/track/#{track_id}?token="+user.token)
      response_body['id'].should == track_id
      response_body['real_time'].should == false

      response_body, response = JsonRestClient.get("/user/#{user.id}/tracks?token="+user.token)
      response_body['tracks'].count.should > 0

    end
  end
end
