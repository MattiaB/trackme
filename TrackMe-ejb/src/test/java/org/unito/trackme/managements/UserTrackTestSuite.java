/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.managements;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Mattia Bertorello
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({org.unito.trackme.managements.UsersManagementTest.class, org.unito.trackme.managements.TrackManagementTest.class, org.unito.trackme.managements.ChallengeManagementTest.class})
public class UserTrackTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
