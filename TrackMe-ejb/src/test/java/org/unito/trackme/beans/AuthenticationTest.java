/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.beans;

import java.util.HashMap;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.persistence.NoResultException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unito.trackme.entities.User;

/**
 *
 * @author Mattia Bertorello Bertorello
 */
public class AuthenticationTest {

    private static Context context;
    private Authentication instance;
    private UserFacade userFacade;

    public AuthenticationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        context = UserTrackTestSuite.getContext();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws NamingException {
        instance = (Authentication) context.lookup("java:global/classes/Authentication");
        userFacade = (UserFacade) context.lookup("java:global/classes/UserFacade");
        try {
            for (User user : userFacade.findAll()) {
                userFacade.remove(user);
            }
        } catch (NoResultException e) {
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of login method, of class Authentication.
     */
    @Test
    public void testSingUpLogin() throws Exception {
        System.out.println("login");
        HashMap<String, String> values = new HashMap<String, String>();
        User newUser = new User("test1", "password");
        userFacade.create(newUser);
        String expToken = newUser.getToken();
        String token = instance.login(newUser.getUsername(), newUser.getPassword()).getToken();
        assertEquals("I due token devono essere uguali", expToken, token);
        String ex = null;
        try {
            instance.login(newUser.getUsername(), newUser.getPassword() + "notCorrect");
        } catch (Exception e) {
            ex = e.getMessage();
        }
        assertEquals("La password deve risultare sbagliata", "Password is not correct", ex);
        try {
            instance.login(newUser.getUsername() + "notExist", newUser.getPassword());
        } catch (Exception e) {
            ex = e.getMessage();
        }
        assertEquals("L'utente non deve esistere", "User does not exist", ex);
    }

    /**
     * Test of tokenExists method, of class Authentication.
     */
    @Test
    public void testTokenExists() throws Exception {
    }
}
