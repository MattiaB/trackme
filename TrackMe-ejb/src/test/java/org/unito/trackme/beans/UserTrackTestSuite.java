/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.beans;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Mattia Bertorello
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({org.unito.trackme.beans.UserFacadeTest.class, org.unito.trackme.beans.AuthenticationTest.class})
public class UserTrackTestSuite {

    private static EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();

    public static Context getContext() {
        return container.getContext();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        container.close();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
