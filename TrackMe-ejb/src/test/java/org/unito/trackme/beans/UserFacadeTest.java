/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.beans;

import org.unito.trackme.beans.UserFacade;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.persistence.NoResultException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.unito.trackme.entities.User;

/**
 *
 * @author Mattia Bertorello Bertorello
 */
public class UserFacadeTest {

    private static Context container;
    private UserFacade instance;

    public UserFacadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        container = UserTrackTestSuite.getContext();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        instance = (UserFacade) container.lookup("java:global/classes/UserFacade");
        try {
            for (User user : instance.findAll()) {
                instance.remove(user);
            }
        } catch (NoResultException e) {
        }
    }

    @After
    public void tearDown() {
    }

    private User createUserFrom(String username, String password) {
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);
        instance.create(newUser);
        return newUser;
    }

    @Test
    public void testFindByUsername() {
        System.out.println("findByUsername");
        String username = "username2";
        String password = "password";
        User createdUser = null;
        createdUser = createUserFrom(username, password);
        assertNotNull("Lo user deve essere creato", createdUser);
        User findedUser = instance.findByUsername(username);
        assertEquals("L'oggetto trovato deve essere quello creato", createdUser.getId(), findedUser.getId());
        findedUser = instance.findByUsername(username + "notE");
        assertNull("L'utente non essere trovato", findedUser);
    }

    @Test
    public void testRemove() {
        System.out.println("remove");
        String username = "username3";
        String password = "password";
        createUserFrom(username, password);
        User delUser = instance.findByUsername(username);
        instance.remove(delUser);
        User findedUser;
        findedUser = instance.findByUsername(username + "notE");
        assertNull("L'utente non essere trovato", findedUser);
    }

    @Test
    public void testUserExist() {
        System.out.println("userExist");
        String username = "username4";
        String password = "password";
        createUserFrom(username, password);
    }
    
    @Test
    public void testCreateAndGetTracks(){
        System.out.println("getTracks");
        createUserFrom("username5", "password");
    }
}
