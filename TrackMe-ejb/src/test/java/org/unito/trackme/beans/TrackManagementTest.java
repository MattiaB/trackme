/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unito.trackme.beans;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.unito.trackme.entities.Point;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.User;
import org.unito.trackme.exception.TrackMeException;
import org.unito.trackme.managements.TrackManagement;
import org.unito.trackme.managements.UsersManagement;

/**
 *
 * @author Mattia Bertorello
 */
public class TrackManagementTest {

    private static Context container;
    private TrackManagement trackManagement;
    private UsersManagement userManagement;

    public TrackManagementTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        container = UserTrackTestSuite.getContext();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws NamingException {
        trackManagement = (TrackManagement) container.lookup("java:global/classes/TrackManagement");
        userManagement = (UsersManagement) container.lookup("java:global/classes/UsersManagement");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreateTrack() {
        try {
            Map<String, String> user = new HashMap<String, String>();
            User newUser = new User("test1", "password");
            try {
                 newUser = userManagement.createUser(newUser);
            } catch (Exception ex) {
                Logger.getLogger(TrackManagementTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            Track newTrack = new Track();
            newTrack.setTitle("Track test");
            newTrack.setTransportType("car");
            newTrack = trackManagement.createTrack(newTrack, newUser.getId());
            
            List<Point> points = new LinkedList<Point>();
            points.add(new Point("2.32","44.34","231","321321432"));
            trackManagement.putPoints(newTrack.getId().toString(), points);
            Track result = trackManagement.getCompleteTrack(newTrack.getId().toString());
            assertEquals("La traccia deve essere in real time", true, result.getRealTime());
            assertNotNull("Ci deve essere qualche punto", result.getPoints());
            trackManagement.putPoints(newTrack.getId().toString(), points);
            result = trackManagement.getCompleteTrack(newTrack.getId().toString());
            System.out.println(result.getPoints());
            trackManagement.endRealTimeTrack(newTrack.getId().toString());
            Track noRealTimeTrack = trackManagement.getCompleteTrack(newTrack.getId().toString());
            assertEquals("La traccia non deve essere real time", false, noRealTimeTrack.getRealTime());
        } catch (TrackMeException ex) {
            Logger.getLogger(TrackManagementTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
