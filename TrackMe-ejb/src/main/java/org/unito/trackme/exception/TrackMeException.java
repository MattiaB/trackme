package org.unito.trackme.exception;

/**
 * Generic TrackMe Exception.
 */
public class TrackMeException extends Exception {

    public TrackMeException() {
        super();
    }

    public TrackMeException(String msg) {
        super(msg);
    }
    
}
