package org.unito.trackme.exception;

/**
 * TrackMe Authentication Exception.
 */
public class AuthenticationException extends Exception {
    
    public AuthenticationException() {
        super();
    }

    public AuthenticationException(String msg) {
        super(msg);
    }

}
