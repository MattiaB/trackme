package org.unito.trackme.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Maps on the Persistence Unit a Trackme Challenge.
 */
@Entity
@Table(name = Challenge.tableName)
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
})
public class Challenge implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String tableName = "CHALLENGES";
    
    /**
     * The challenge's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    /**
     * The title of the challenge.
     */
    @Column(nullable = true)
    private String title;
    
    /**
     * The challenge's description.
     */
    @Column(nullable = true)
    private String description;
    
    /**
     * The transport-type constraint
     * of all the challenge's tracks.
     */
    @Column(nullable = false)
    private String transportType;
    
    /**
     * The challenge's creation date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date creationDate;
    
    /**
     * The number of day of lifetime of the challenge.
     */
    @Column(nullable = false)
    private Long lifeTime;
    
    /**
     * The challenge's main track.
     */
    @OneToOne(optional=false, mappedBy="mainTrackToChallenge")
    @XmlTransient
    private Track mainTrack;
    
    /**
     * The list of the challenge's submitted tracks. 
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "challenge")
    @XmlTransient
    private List<Track> challengesTracks;
    
    /**
     * The creator of the challenge.
     */
    @ManyToOne
    @JoinColumn(name = "OWNER_ID", nullable = false)
    @XmlTransient
    private User owner;
    
    @PrePersist
    protected void prePersist() {
        this.setCreationDate(new Date());
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public Track getMainTrack() {
        return mainTrack;
    }

    public void setMainTrack(Track mainTrack) {
        this.mainTrack = mainTrack;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<Track> getChallengesTracks() {
        return challengesTracks;
    }

    public void setChallengesTracks(List<Track> challengesTracks) {
        this.challengesTracks = challengesTracks;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
    
    public Long getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(Long lifeTime) {
        this.lifeTime = lifeTime;
    }
  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Challenge)) {
            return false;
        }
        Challenge other = (Challenge) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.unito.trackme.entities.Challenge[ id=" + id + " ]";
    }

    public Map toMap() {
        Map<String, String> result = new HashMap<String, String>();
        result.put("id", this.getId().toString());
        result.put("title", this.getTitle());
        result.put("description", this.getDescription());
        result.put("transport_type", this.getTransportType());
        result.put("creation_date", this.getCreationDate().toString());
        result.put("life_time", this.getLifeTime().toString());
        return result;
    }
}
