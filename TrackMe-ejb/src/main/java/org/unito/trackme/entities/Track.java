package org.unito.trackme.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Maps on the Persistence Unit a GPS Track.
 */
@Entity
@Table(name = Track.tableName)
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries({
    @NamedQuery(name = "Track.findAll", query = "SELECT t FROM Track t"),
    @NamedQuery(name = "Track.findById", query = "SELECT t FROM Track t WHERE t.id = :id")})
public class Track implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String tableName = "TRACKS";
    
    /**
     * The track's unique id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
     
    /**
     * The track's title.
     */  
    @Column(nullable = false)
    private String title;
    
    /**
     * The creation date of the Track.
     */ 
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date creationDate;
    
    /**
     * The list of the Track's GPS locations.
     */ 
    @ElementCollection
    @CollectionTable(
        name = "TRACK_POINTS",
    joinColumns =
    @JoinColumn(name = "TRACK_ID"))
    @Column(nullable = true)
    private List<Point> points = new ArrayList<Point>();
    
    /**
     * The type of transport used while recording the track.
     */
    @Column(nullable = false)
    private String transportType;
    
    /**
     * Boolean flag indicating whether the track is in RealTime or not.
     */
    @Column(nullable = false)
    private Boolean realTime;
    
    /**
     * The creator of the Track.
     */
    @ManyToOne
    @JoinColumn(name = "OWNER_ID", nullable = false)
    @XmlTransient
    private User owner;
    
    /**
     * The challenge whom the track belongs to.
     */
    @ManyToOne
    @JoinColumn(name = "CHALLENGE_ID", nullable = true)
    @XmlTransient
    private Challenge challenge;
    
    /**
     * The Challenge that has as MainTrack this Track.
     */
    @OneToOne(optional = true)    
    @JoinColumn(
      name = "MAINTRACKTOCHALLENGE_ID", nullable = true)
    @XmlTransient   
    private Challenge mainTrackToChallenge;

    public Track() {
        this.realTime = true;
    }

    public Track(Long id) {
        this.id = id;
    }

    @PrePersist
    protected void prePersist() {
        this.setRealTime(Boolean.TRUE);
        this.setCreationDate(new Date());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public Boolean getRealTime() {
        return realTime;
    }

    public void setRealTime(Boolean realTime) {
        this.realTime = realTime;
    }

    public Challenge getChallenge() {
        return challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Challenge getMainTrackToChallenge() {
        return mainTrackToChallenge;
    }

    public void setMainTrackToChallenge(Challenge mainTrackToChallenge) {
        this.mainTrackToChallenge = mainTrackToChallenge;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Track)) {
            return false;
        }
        Track other = (Track) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.unito.trackme.entities.Track[ id=" + id + " ]";
    }

    public Map toMap() {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.getId());
        result.put("title", this.getTitle());
        result.put("created_time", this.getCreationDate().toString());
        result.put("real_time", this.getRealTime());
        result.put("transport_type", this.getTransportType());
        return result;
    }
}
