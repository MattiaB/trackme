package org.unito.trackme.entities;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * A simple GPS Location.
 */
@Embeddable
public class Point implements Serializable {

    /**
     * The GPS latitude.
     */
    private String latitude;
    
    /**
     * The GPS longitude.
     */
    private String longitude;
    
    /**
     * The GPS elevation.
     */
    private String elevation;
    
    /**
     * The time in which the location has been recorded.
     */
    private String time;

    public Point() { }

    public Point(String latitude, String longitude, String elevation, String time) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.elevation = elevation;
        this.time = time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getElevation() {
        return elevation;
    }

    public void setElevation(String elevation) {
        this.elevation = elevation;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
