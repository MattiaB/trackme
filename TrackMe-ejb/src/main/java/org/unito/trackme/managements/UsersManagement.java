package org.unito.trackme.managements;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.beans.Authentication;
import org.unito.trackme.beans.UserFacade;
import org.unito.trackme.entities.Challenge;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.User;
import org.unito.trackme.exception.AuthenticationException;
import org.unito.trackme.exception.TrackMeException;

/**
 * Provides some methods to manage Users and their authentication.
 */
@Stateless
@LocalBean
public class UsersManagement {

    private static final Logger logger = LogManager.getLogger(UsersManagement.class);
    
    /**
     * The bean used to manage user's authentication.
     */
    @EJB
    private Authentication authentication;
    
    /**
     * The User entity's Facade.
     */
    @EJB
    private UserFacade userFacade;

    
    // TODO: si potrebbe rimuovere il metodo obbligando le classi "client" 
    //       a utilizzare esplicitamente l'authentication manager
    
    /**
     * Authenticates the User with the given credentials.
     * 
     * @param username the user's username.
     * @param password the user's password.
     * @return the authenticated User.
     * @throws TrackMeException if the specified paramethers are not consistent.
     * @throws AuthenticationException if the authentication credentials are not valid.
     */
    public User login(String username, String password) throws TrackMeException, AuthenticationException {

        return authentication.login(username, password);

    }

    /**
     * Returns an instance of the User entity with the specified id.
     * 
     * @param id the requested user's id.
     * @return the requested Users.
     * @throws TrackMeException if the user does not exists.
     */
    public User getUser(String id) throws TrackMeException {
        
        logger.info("Cerco l'utente con id " + id);
        
        checkUserId(id);
        try {
            return userFacade.find(id);
        } catch(Exception e) {
            throw new TrackMeException("Invalid user id.");
        }
    
    }

    /**
     * Create a record in the Databse for the given User.
     * 
     * @param newUser the User that has to be created.
     * @return the persisted User's entity.
     * @throws TrackMeException if there is another user with the same username or email.
     */
    public User createUser(User newUser) throws TrackMeException {
        
        logger.info("Si cerca di creare l'utente con username: " + newUser.getUsername());
        logger.info("L'utente esiste già: " + userFacade.userExists(newUser.getUsername()));
        
        if (userFacade.userExists(newUser.getUsername())) {
            logger.info("L'utente esiste già " + newUser.getUsername());
            throw new TrackMeException("Invalid username is already exists.");
        }
        
        userFacade.create(newUser);
        logger.info("User id: " + newUser.getId());
        return newUser;
    
    }

    /**
     * Returns the list of the Tracks created by the specified User.
     * 
     * @param id the ID of the specified User.
     * @return The List of the Tracks created by the User.
     * @throws TrackMeException if the user does not exists or if the id is not consistent.
     */
    public List<Track> getTracks(String id) throws TrackMeException {
        
        checkUserId(id);

        if (!userFacade.userExists(Long.parseLong(id))) {
            logger.info("L'utente con id " + id + " non è stato trovato");
            throw new TrackMeException("Invalid user id.");
        }
        
        List<Track> results = userFacade.getTracks(id);
        logger.info("Richiesta delle traccie dell'utente " + id + " restituite " + results.size() + " tracce");
        return results;
    
    }

    /**
     * Returns the list of the Challenges joined or created by the specified User.
     * 
     * @param id the ID of the specified User.
     * @return The List of the Challenges joined or created by the User.
     * @throws TrackMeException if the user does not exists or if the id is not consistent.
     */
    public List<Challenge> getChallenges(String id) throws TrackMeException {
        
        checkUserId(id);
   
        if (!userFacade.userExists(Long.parseLong(id))) {
            logger.info("L'utente con id " + id + " non è stato trovato");
            throw new TrackMeException("Invalid user id.");
        }
        
        List<Challenge> results = userFacade.getChalleges(id);
        logger.info("Richiesta delle sfide dell'utente " + id + " restituite " + results.size() + " sfide");
        return results;
    }

    /**
     * Checks the consistency of the id.
     * 
     * @param id the id that as to be checked.
     * @throws TrackMeException if the id is null.
     */
    private void checkUserId(String id) throws TrackMeException {
        
        if (id == null || id.equals("")) {
            logger.info("L'utente ha messo un user id nullo");
            throw new TrackMeException("Please specify the user id.");
        }
    
    }
}
