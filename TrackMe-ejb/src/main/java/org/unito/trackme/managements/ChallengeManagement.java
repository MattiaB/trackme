package org.unito.trackme.managements;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.beans.ChallengeFacade;
import org.unito.trackme.entities.Challenge;
import org.unito.trackme.entities.Track;
import org.unito.trackme.exception.TrackMeException;

/**
 * Provides methods to manipulate Challenges and Challenges Tracks.
 */
@Stateless
@LocalBean
public class ChallengeManagement {

    private static final Logger logger = LogManager.getLogger(ChallengeManagement.class);
    
    /**
     * The Challenge entity's Facade.
     */
    @EJB
    private ChallengeFacade challengeFacade;

    /**
     * Persists the given Challenge.
     * 
     * @param newChallenge the Challenge that has to be persisted.
     * @return the persisted Challenge.
     * @throws TrackMeException if the Challenge's mainTrack is null.
     */
    public Challenge createChallenge(Challenge newChallenge) throws TrackMeException {
        
        logger.info("L'utente creare una sfida: " + newChallenge.getTitle()
                + "con main track " + newChallenge.getMainTrack().getId());
        
        if (newChallenge.getMainTrack().getId() == null) {
            throw new TrackMeException("Invalid main track id for challenge.");
        }
        challengeFacade.create(newChallenge);
        
        return newChallenge;
    
    }

    /**
     * Returns the list of all the Challenges in the Persistece Unit.
     * 
     * @return the list of all the persisted Challenges.
     */
    public List<Challenge> getAllChallenge() {
        
        logger.info("L'utente vuole le sfide");
        try {
            return challengeFacade.findAll();
        } catch (Exception e) {
            return new ArrayList<Challenge>();
        }
        
    }

    /**
     * Returns the list of the specified Challenge's Tracks.
     * 
     * @param id the Challenge's id.
     * @return the list of the specified Challenge's Tracks.
     * @throws TrackMeException if the Challenge's id is not valid. 
     */
    public List<Track> getTracksByChallegeId(String id) throws TrackMeException {
        
        checkChallengeId(id);
        
        logger.info("L'utente vuole le tracce della sfida " + id);
        
        List<Track> challengeTracks;
        try {
            challengeTracks = challengeFacade.getTracksByChallengeId(id);
        } catch (Exception e) {
            logger.info("La sfida " + id + "non esiste, metodo: getTracksByChallegeId");
            throw new TrackMeException("Invalid challenge id.");
        }
        
        return challengeTracks;
    
    }

    /**
     * Returns the Challenge with the given id.
     * 
     * @param id the Challenge's id.
     * @return if it exists, the Challenge's with the given id, otherwise null.
     * @throws TrackMeException if the Challenge's id is not valid. 
     */
    public Challenge getCompleteChallenge(String id) throws TrackMeException {
        
        checkChallengeId(id);
        
        logger.info("L'utente vuole tutte le informazioni della sfida " + id);
        
        Challenge challenge;
        try {
            challenge = challengeFacade.find(id);
        } catch (Exception e) {
            logger.info("La sfida " + id + "non esiste, metodo: getCompleteChallenge");
            throw new TrackMeException("Invalid challenge id.");
        }
        
        return challenge;
    
    }

    /**
     * Adds the given Track to the specified Challenge.
     * 
     * @param id the Challenge's id.
     * @param track the Track that has to be added to the Challenge.
     * @throws TrackMeException if the Challenge's id is invalid 
     *                          or the Track is not consistent with the Challenge's 
     *                          MainTrack.
     */
    public void addTrackToChallenge(String id, Track track) throws TrackMeException {
        
        checkChallengeId(id);
        
        logger.info("L'utente vuole aggiungere alla sfida " + id + " la traccia " + track.getId());
        
        try {
            
            // TODO:
            // Verificare che la traccia sia coerente con la Main Track
            
            challengeFacade.addTrackToChallenge(id, track);
        } catch (Exception e) {
            logger.info("La sfida " + id + "non esiste, metodo: addTrackToChallenge");
            throw new TrackMeException("Invalid challenge id.");
        }
    
    }

    /**
     * Checks the consistency of the id.
     * 
     * @param id the id that as to be checked.
     * @throws TrackMeException if the id is null.
     */
    private void checkChallengeId(String id) throws TrackMeException {
        
        if (id == null || id.equals("")) {
            logger.info("L'utente ha messo un challenge id nullo");
            throw new TrackMeException("Please specify the challenge id.");
        }
    
    }
}
