package org.unito.trackme.managements;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.beans.TrackFacade;
import org.unito.trackme.beans.TrackRealTime;
import org.unito.trackme.entities.Point;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.User;
import org.unito.trackme.exception.TrackMeException;

/**
 * Provides methods to manipulate Tracks independently from their
 * status (realtime or ended).
 */
@Stateless
@LocalBean
public class TrackManagement {

    private static final Logger logger = LogManager.getLogger(TrackManagement.class);
    
    /**
     * The Track entity's Facade.
     */
    @EJB
    private TrackFacade trackFacade;
    
    /**
     * The RealTime Tracks management .
     */
    @EJB
    private TrackRealTime trackRealTime;

    /**
     * Returns a Track given its id.
     * 
     * @param trackId the id of the Track.
     * @return the Track with the given id.
     * @throws TrackMeException if the TrackId is invalid.
     */
    public Track getCompleteTrack(String trackId) throws TrackMeException {
        
        checkTrackId(trackId);
        
        Track track = null;
        try {
            track = trackFacade.find(trackId);
        } catch (Exception e) {
            logger.info("La traccia " + trackId + "non esiste");
            throw new TrackMeException("Invalid track id.");
        }
        
        track.setPoints(getPoints(trackId));
        logger.info("Restituzione della traccia " + trackId + " completa di punti");
        
        return track;
    
    }

    /**
     * Returns the List of points belonging to the specified Track.
     * 
     * @param trackId the id of the Track.
     * @return the list of the Track's points.
     * @throws TrackMeException if the TrackId is invalid.
     */
    public List<Point> getPoints(String trackId) throws TrackMeException {
        
        checkTrackId(trackId);
        
        List<Point> points = trackRealTime.getPoints(trackId);
        if (points == null) {
            
            try {
                points = trackFacade.getPoints(trackId);
            } catch (Exception e) {
                logger.info("La traccia " + trackId + "non esiste");
                throw new TrackMeException("Invalid track id.");
            }
            
            trackRealTime.addPointToTrack(trackId, points);
            trackRealTime.setExpirePoints(trackId);
        
        }
        
        logger.info("Restituzione dei punti della traccia " + trackId + "in numero: " + points.size());
        
        return points;
    
    }

    /**
     * Persist the given Track.
     * 
     * @param newTrack the Track that has to be persisted.
     * @param ownerId the owner of the Track.
     * @return the persisted entity of the given Track
     */
    public Track createTrack(Track newTrack, Long ownerId) {
        
        newTrack.setOwner(new User(ownerId));
        trackFacade.create(newTrack);
        trackRealTime.addPointToTrack(newTrack.getId().toString(), new LinkedList<Point>());
        
        //TODO Inserire altri controlli di eccezioni 
        return newTrack;
    
    }

    /**
     * 
     * @param trackId
     * @param points
     * @throws TrackMeException 
     */
    public void putPoints(String trackId, Collection<Point> points) throws TrackMeException {
        checkTrackId(trackId);
        try {
            if (!trackRealTime.existTrack(trackId)) {
                trackFacade.find(trackId);
            }
        } catch (Exception e) {
            logger.info("Si cerca di aggiungere dei punti ad una traccia che non esiste nel metodo putPoints");
            throw new TrackMeException("Invalid track id.");
        }
        trackRealTime.addPointToTrack(trackId, points);

        logger.info("Aggiunti " + points.size() + " punti alla traccia " + trackId);
    }

    public void endRealTimeTrack(String trackId) throws TrackMeException {
        checkTrackId(trackId);
        List<Point> points;
        points = trackRealTime.getPoints(trackId);
        if (points == null) {
            logger.info("Si cerca di terminare una traccia che non è real time");
            throw new TrackMeException("The track is not in real time.");
        }
        try {
            trackFacade.setPointsAndRealTimeFalse(trackId, points);
        } catch (Exception e) {
            logger.info("Si cerca di aggiungere dei punti ad una traccia che non esiste nel metodo endRealTimeTrack");
            throw new TrackMeException("Invalid track id.");
        }
        trackRealTime.setExpirePoints(trackId);
    }

    /**
     * Checks the consistency of the id.
     * 
     * @param id the id that as to be checked.
     * @throws TrackMeException if the id is null.
     */
    private void checkTrackId(String id) throws TrackMeException {
        
        if (id == null || id.equals("")) {
            logger.info("L'utente ha messo un track id nullo");
            throw new TrackMeException("Please specify the track id.");
        }
    
    }
}
