package org.unito.trackme.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Provides an abstraction of the operation on
 * the Redis key-value DB.
 */
@Singleton
@LocalBean
public class KeyValueBean {

    /**
     * The connection to the Redis pool.
     */
    private JedisPool pool;

    @PostConstruct
    void init() {
        pool = new JedisPool("localhost");
    }

    @PreDestroy
    public void destroyPool() {
        /// ... when closing your application:
        pool.destroy();
    }

    /**
     * Appends the given value to the value set of the specified key.
     * 
     * @param key the search key.
     * @param value the value that has to be added.
     */
    public void append(String key, String value) {
        Jedis jedis = pool.getResource();
        try {
            jedis.append(key, value);
        } finally {
            /// ... it's important to return the Jedis instance to the pool once you've finished using it
            pool.returnResource(jedis);
        }
    }

    /**
     * Checks if the given key exists in the key-value DB.
     * 
     * @param key the search key.
     * @return true if the key is present in the key-value DB,
     *         otherwise false.
     */
    public boolean exists(String key) {
        Jedis jedis = pool.getResource();
        boolean result = false;
        try {
            result = jedis.exists(key);
        } finally {
            pool.returnResource(jedis);
        }
        return result;
    }

    /**
     * Returns the value associated to the given key.
     * 
     * @param key the search key.
     * @return the value associated to the key if it is present in the key-value DB,
     *         otherwise "".
     */
    public String get(String key) {
        Jedis jedis = pool.getResource();
        String result = "";
        try {
            result = jedis.get(key);
        } finally {
            pool.returnResource(jedis);
        }
        return result;
    }

    /**
     * Sets the value associated to the given key.
     * 
     * @param key the search key.
     * @param value the value that has to be associated to the key.
     */
    public void set(String key, String value) {
        Jedis jedis = pool.getResource();
        String result = "";
        try {
            result = jedis.set(key, value);
        } finally {
            pool.returnResource(jedis);
        }
    }

    /**
     * Removes the given key from the key-value DB.
     * 
     * @param key the search key.
     */
    public void delete(String key) {
        Jedis jedis = pool.getResource();
        try {
            jedis.del(key);
        } finally {
            pool.returnResource(jedis);
        }
    }

    /**
     * Sets the expire date of the specified key.
     * 
     * @param key the search key.
     * @param seconds the lifetime of the key in seconds.
     */
    public void expire(String key, int seconds) {
        Jedis jedis = pool.getResource();
        try {
            jedis.expire(key, seconds);
        } finally {
            pool.returnResource(jedis);
        }
    }

    /**
     * Increments the value associated to the specified key.
     * 
     * @param key the search key.
     */
    public void incr(String key) {
        Jedis jedis = pool.getResource();
        try {
            jedis.incr(key);
        } finally {
            pool.returnResource(jedis);
        }
    }
}
