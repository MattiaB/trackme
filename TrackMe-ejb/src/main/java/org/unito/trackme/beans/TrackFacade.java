package org.unito.trackme.beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.entities.Point;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.User;

/**
 * Provides an abstraction of the operation over the Track entity.
 */
@Stateless
public class TrackFacade extends AbstractFacade<Track> {

    private static final Logger logger = LogManager.getLogger(TrackFacade.class);
    
    /**
     * The entity manager used to query the Persistence Unit.
     */
    @PersistenceContext(unitName = "org.unito_TrackMe-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Creates a new instance of TrackFacade.
     */
    public TrackFacade() {
        super(Track.class);
    }
    
    /**
     * Returns the Persistence Unit's entity manager.
     * 
     * @return the EntityManager.
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Returns the Track with the given id.
     * 
     * @param id the id of the Track.
     * @return if the Peristence Unit cointains a Track with the
     *         given id it returns an instance of that Track, otherwise null.
     */
    public Track find(String id) {
        
        logger.info("Richiesta traccia " + id);
        
        return super.find(Long.valueOf(id));
    
    }

    /**
     * Returns the list of the specified Track's Points.
     * 
     * @param trackId the id of the Track.
     * @return the list of the Track's Points.
     */
    public List<Point> getPoints(String trackId) {
        
        logger.info("Richiesta dei punti per la traccia " + trackId);
        
        List<Point> results = find(trackId).getPoints();
        
        logger.info("Punti restituiti per la traccia " + trackId + ": " + results.size());
        
        return results;
    
    }

    /**
     * Returns the track's creator.
     * 
     * @param trackId the id of the Track.
     *
     * @return if the Peristence Unit cointains a Track with the
     *         given id it returns an instance of its creator, otherwise null.
     */
    public User getUser(String trackId) {
        
        logger.info("Richiesta informazioni utente per la traccia " + trackId);
        
        Track track = find(trackId);
        
        return track == null ? null : track.getOwner();
    
    }

    /**
     * Add to the specified track the given points and set it as completed.
     * 
     * @param trackId the id of the Track.
     * @param points the list of points that has to be added to the track.
     */
    public void setPointsAndRealTimeFalse(String trackId, List<Point> points) {
        
        logger.info("Richiesta terminazione del realtime per la  traccia " + trackId
                + " con " + points.size() + "punti");
        
        Track dbTrack = find(trackId);
        
        if(dbTrack != null) {
            dbTrack.setPoints(points);
            dbTrack.setRealTime(Boolean.FALSE);
            super.edit(dbTrack);
        }
        else {
            throw new IllegalArgumentException("There is no Track with id " + trackId);
        }
    
    }

}
