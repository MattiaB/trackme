package org.unito.trackme.beans;

import java.util.List;
import javax.persistence.EntityManager;

/**
 * Abstract class to implement the Facade Design Pattern over
 * Persistence Unit's entities.
 * 
 * @param <T> the class to wrap with the facade.
 */
public abstract class AbstractFacade<T> {
    
    /**
     * The class wrapped by the facede.
     */
    private Class<T> entityClass;
    
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
    
    /**
     * Returns the Persistence Unit's entity manager.
     * 
     * @return the EntityManager.
     */
    protected abstract EntityManager getEntityManager();

    /**
     * Persists the given instance of T.
     * 
     * @param entity the instance of T that has to be persisted.
     */
    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    /**
     * Persists the chenges in the given instance of T.
     * 
     * @param entity the instance of T that has to be merged.
     */
    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    /**
     * Removes the given instance of T from the Persistence Unit.
     * 
     * @param entity the instance of T that has to be removed.
     */
    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    /**
     * Look for an instance of T with the given id.
     * 
     * @param id the looked for instance's id.
     * @return an instance of T.
     */
    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    /**
     * Find all the persisted instances of T.
     * 
     * @return the list of the persisted instances of T.
     */
    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    /**
     * Find all the persisted instances of T in the given range of values.
     * 
     * @param range the range of values that has to be returned.
     * @return the list of the persisted instances of T in the given range.
     */
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    /**
     * Returns the number of persisted instances of T.
     * 
     * @return the number of persisted instances of T.
     */
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
