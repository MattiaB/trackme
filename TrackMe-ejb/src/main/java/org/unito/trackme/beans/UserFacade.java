package org.unito.trackme.beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.entities.Challenge;
import org.unito.trackme.entities.Challenge_;
import org.unito.trackme.entities.Track;
import org.unito.trackme.entities.Track_;
import org.unito.trackme.entities.User;
import org.unito.trackme.entities.User_;

/**
 * Provides an abstraction of the operation over the User entity.
 */
@Stateless
public class UserFacade extends AbstractFacade<User> {

    private static final Logger logger = LogManager.getLogger(UserFacade.class);
    
    /**
     * The entity manager used to query the Persistence Unit.
     */
    @PersistenceContext(unitName = "org.unito_TrackMe-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Creates a new instance of UserFacade.
     */
    public UserFacade() {
        super(User.class);
    }
    
    /**
     * Returns the Persistence Unit's entity manager.
     * 
     * @return the EntityManager.
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Returns the user with the given id.
     * 
     * @param id the id of the user.
     * @return if the Peristence Unit cointains a user with the
     *         given id it returns an instance of that User, otherwise null.
     */
    public User find(String id) {
        
        logger.info("Richiesta user " + id);
        
        User result = super.find(Long.valueOf(id));
        if (result != null) {
            logger.info("Trovato dall'id" + id + " l'user: " + result.getUsername());

        } else {
            logger.info("L'utente " + id + " cercato non è stato trovato");
        }
        
        return result;
    
    }

    /**
     * Returns the user with the given username.
     * 
     * @param username the username of the user.
     * @return the instance User with the given username, otherwise null.
     */
    public User findByUsername(String username) {
        
        logger.info("Cercato l'user con username: " + username);
        
        Query userNameQuery = em.createNamedQuery("User.findByUsername");
        userNameQuery.setParameter("username", username);
        
        logger.info("Sto per eseguire la query: " + userNameQuery.toString());
        
        User result = (User) userNameQuery.getSingleResult();
        
        logger.info("Trovato user: username=" + username + ", id=" + result.getId().toString());
        
        return result;
    
    }
    
    /**
     * Returns the user with the given username and password.
     * 
     * @param username the user's username.
     * @param password the user's password.
     * @return the instance User with the given username and password, otherwise null.
     */
    public User findByUsernameAndPassword(String username, String password) {
       
        logger.info("Cercato l'user con username: " + username + " e password: " + password);
        
        Query userNameQuery = em.createNamedQuery("User.findByUsernameAndPassword");
        userNameQuery.setParameter("username", username);
        userNameQuery.setParameter("password", password);
        
        logger.info("Sto per eseguire la query: " + userNameQuery.toString());
        
        User result = (User) userNameQuery.getSingleResult();
        
        logger.info("Trovato user: username=" + username + ", id=" + result.getId().toString());
        
        return result;
    }

    /**
     * Returns the list of the Tracks of the specified User.
     * 
     * @param id the user's id.
     * @return the list of the track of the users with the specified id.
     */
    public List<Track> getTracks(String id) {
        
        logger.info("Richiesta delle traccie dell'utente: " + id);

        // Query conditions
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Track> cq = qb.createQuery(Track.class);
        Root<Track> t = cq.from(Track.class);
        Predicate condition = qb.equal(t.get(Track_.owner), new User(Long.parseLong(id)));
        cq.where(condition);
        
        TypedQuery<Track> q = em.createQuery(cq);
        List<Track> result = q.getResultList();

        logger.info("Id utente: " + id + " numero di tracks trovate: " + result.size());
        
        return result;
    
    }

    /**
     * Returns the list of the Challenges of the specified User.
     * 
     * @param id the user's id.
     * @return the list of the challenges of the users with the specified id.
     */
    public List<Challenge> getChalleges(String id) {
        
        logger.info("Richiesta delle sfide dell'utente: " + id);

        // Query conditions
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Challenge> cq = qb.createQuery(Challenge.class);
        Root<Challenge> c = cq.from(Challenge.class);
        Predicate condition = qb.equal(c.get(Challenge_.owner), new User(Long.parseLong(id)));
        cq.where(condition);
        
        TypedQuery<Challenge> q = em.createQuery(cq);
        List<Challenge> result = q.getResultList();

        logger.info("Id utente: " + id + " numero di challenges trovate: " + result.size());
        
        return result;
    }

    /**
     * Deletes the User with the given id.
     * 
     * @param id the ID of the user that has to be removed
     */
    public void remove(String id) {
        
        logger.info("Rimosso utente con Id: " + id);
        User user = find(id);
        super.remove(user);
    
    }

    /**
     * Checks if already exists in the DB an User with the given Id.
     * 
     * @param id the Id that has to be checked.
     * @return true if the exists a user with the given id, otherwise false.
     */
    public boolean userExists(Long id) {
        
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<User> cq = qb.createQuery(User.class);
        
        Root<User> c = cq.from(User.class);             // From       
        cq.where(qb.equal(c.get(User_.id), id));       // Where
        
        TypedQuery<User> q = em.createQuery(cq);
        
        logger.info("Risultato userExists con Id: " + id + " results: " + (q.getResultList().size() == 1));
        
        return q.getResultList().size() == 1;
    
    }
    
    /**
     * Checks if already exists in the DB at least a User 
     * with the username or the email equals to the given string.
     * 
     * @param string the string that has to be checked.
     * @return true if there is at least a user that 
     *         with the username or the email equals to the given string.
     */
    public boolean userExists(String string) {
        
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<User> cq = qb.createQuery(User.class);
        
        Root<User> c = cq.from(User.class);                     // From
        
        // Conditions
        Predicate[] conditions = {                              // Where
            qb.equal(c.get(User_.username), string.trim()),
            qb.equal(c.get(User_.email), string.trim())
        };
        cq.where(qb.or(conditions));  
        
        TypedQuery<User> q = em.createQuery(cq);
        
        logger.info("Risultato userExists con username or email: " + string + " results: " + (q.getResultList().size() >= 1));
        
        return q.getResultList().size() >= 1;
    
    }
    
}
