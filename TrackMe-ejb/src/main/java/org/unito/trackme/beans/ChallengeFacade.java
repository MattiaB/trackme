package org.unito.trackme.beans;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.entities.Challenge;
import org.unito.trackme.entities.Track;

/**
 * Provides an abstraction of the operation over the Challenge entity.
 */
@Stateless
public class ChallengeFacade extends AbstractFacade<Challenge> {

    private static final Logger logger = LogManager.getLogger(ChallengeFacade.class);
    
    /**
     * The entity manager used to query the Persistence Unit.
     */
    @PersistenceContext(unitName = "org.unito_TrackMe-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    /**
     * Creates a new instance of ChallengeFacade.
     */
    public ChallengeFacade() {
        super(Challenge.class);
    }
    
    /**
     * Returns the Persistence Unit's entity manager.
     * 
     * @return the EntityManager.
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * Returns the Challenge with the given id.
     * 
     * @param id the id of the Challenge.
     * @return if the Peristence Unit cointains a Challenge with the
     *         given id it returns an instance of that Challenge, otherwise null.
     */
    public Challenge find(String id) {
        
        logger.info("Richiesta challenge " + id);
        
        return super.find(Long.valueOf(id));

    }

    /**
     * Persists the given Challenge.
     * 
     * @param newChallenge the challenge entity that has to be persisted.
     */
    @Override
    public void create(Challenge newChallenge) {
        
        Track mainTrack = getEntityManager().find(Track.class, newChallenge.getMainTrack().getId());
//        newChallenge.setMainTrack(mainTrack);
//        newChallenge.setTransportType(mainTrack.getTransportType());
        super.create(newChallenge);
        mainTrack.setMainTrackToChallenge(newChallenge);
        getEntityManager().merge(mainTrack);

    }

    /**
     * Returns the list of the specified Challenge's Tracks.
     * 
     * @param id the id of the Challenge.
     * @return the list of the specified challenge's tracks.
     */
    public List<Track> getTracksByChallengeId(String id) {
        
        logger.info("Richieste tracce della sfida " + id);
        
        Challenge challenge = find(id);
        
        if(challenge != null) {
            
            List<Track> results = challenge.getChallengesTracks();
            
            logger.info("Tracce trovate della sfida " + id + ": " + results.size());
            
            return results;
        
        }
        else {
            return new ArrayList<Track>();
        }
    
    }

    /**
     * Adds the given Track to the specified Challenge.
     * 
     * @param id the id of the Challenge.
     * @param track the Track that has to be added.
     */
    public void addTrackToChallenge(String id, Track track) {
        
        Challenge challenge = find(id);
        
        if(challenge != null) {
            List<Track> tracks = challenge.getChallengesTracks();
            track = getEntityManager().find(Track.class, track.getId());
            tracks.add(track);
            challenge.setChallengesTracks(tracks);
            track.setChallenge(challenge);
            getEntityManager().merge(track);
            super.edit(challenge);
        }
        else {
            throw new IllegalArgumentException("There is no Challenge with id " + id);
        }
    
    }

}
