package org.unito.trackme.beans;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.entities.User;
import org.unito.trackme.exception.AuthenticationException;
import org.unito.trackme.exception.TrackMeException;

/**
 * Bean class used to manage the User's authentication.
 */
@Stateless
@LocalBean
public class Authentication {

    private static final Logger logger = LogManager.getLogger(Authentication.class);
    
    /**
     * The User entity facade.
     */
    @EJB
    private UserFacade userFacade;
    
    /**
     * The KeyValue DB Bean.
     */
    @EJB
    private KeyValueBean dbKeyValue;
    
    /**
     * Lifetime of the API token.
     */
    private static final int day = 86400;
    
    /**
     * Redis prefix to identify the users's informations.
     */
    private static final String preKey = "u:";

    /**
     * Authenticates the users within the system.
     * 
     * @param username The user's username.
     * @param password The user's password.
     * @return A User entity modelling all the informations of authenticated user.
     * @throws TrackMeException if the given username or password are not valid. 
     * @throws AuthenticationException if the given username or password do not exist.
     */
    public User login(String username, String password) throws AuthenticationException, TrackMeException {
        
        // Checking the consistency of the input parameters.
        
        if (username == null || username.equals("")) {
            logger.info("L'user ha messo una username nullo");
            throw new TrackMeException("Please provide the username.");
        }
        if (password == null || password.equals("")) {
            logger.info("L'user " + username + " ha messo una password nulla ");
            throw new TrackMeException("Please provide the password.");
        }
     
        logger.info("L'utente " + username
                + " sta cercando di fare il login con questa password: " + password);
        
        User userEntity = null;
        try {
            userEntity = userFacade.findByUsernameAndPassword(username, password);
        } catch(Exception e) {
            
            logger.warn("Nome utente o password sbagliati!");
            throw new AuthenticationException("Incorrect username or password.");
        
        }
        
        // Now the user is authenticated!
        
        // Adding the user's token to the Redis DB in order
        // to authenticate his future API call.
        String key = preKey + userEntity.getToken();
        dbKeyValue.set(key, userEntity.getId().toString());
        dbKeyValue.expire(key, day);
        
        return userEntity;
    
    }

    /**
     * 
     * Returns the user id of the authenticated User with the given Token
     * 
     * @param token the authentication token that has to be checked.
     * @return Long value >= 0 if the user is present in Redis, -1 otherwise.
     * @throws TrackMeException if the given token is invalid.
     */
    public Long checkToken(String token) throws TrackMeException {
        
        // Verifico la validità dei parametri in input
        if (token == null || token.equals("")) {
            logger.info("L'utente ha messo un token nullo");
            throw new TrackMeException("Please provide a valid API key.");
        }
        
        // Controllo in Redis se l'utente ha già effettuato il login
        String redisUserKey = preKey + token;
        String userId = dbKeyValue.get(redisUserKey);
        if (userId == null) {
            
            logger.info("Il token " + token + " non esiste in redis.");
            return -1l;
        
        } 
        else {
            
            logger.info("Il token " + token + " esiste.");
            return Long.parseLong(userId);
        
        }
    }
    
    /**
     * 
     * Check whether the given token exists in the Redis DB, actually that
     * means that the user is logged in, or not.
     * 
     * @param token the authentication token that has to be checked.
     * @return a boolean value whether the user is logged in or not.
     * @throws TrackMeException if the given token is invalid.
     */
    public boolean tokenExists(String token) throws TrackMeException {
        
        return checkToken(token) > -1;
        
    }
    
}
