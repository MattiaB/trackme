package org.unito.trackme.beans;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.unito.trackme.entities.Point;

/**
 * Provides some primitives to manipulate RealTime Tracks in the Key-Value DB.
 */
@Stateless
public class TrackRealTime {

    private static final Logger logger = LogManager.getLogger(TrackRealTime.class);
    
    private static final String preKey = "t:";
    private static final int hour = 3600;
    
    /**
     * The key-value DB management bean. 
     */
    @EJB
    private KeyValueBean dbKeyValue;

    /**
     * Returns the Point list of the specified Real-Time Track
     * 
     * @param trackId the id of the specified Realtime Track
     * @return 
     */
    public List<Point> getPoints(String trackId) {
        
        String redisKeyTrack = getRedisKeyTrack(trackId);
        String rawJsonTrack = dbKeyValue.get(redisKeyTrack);
        
        if (rawJsonTrack != null) {
            
            if (rawJsonTrack.length() > 21) {
                
                logger.info("Stringa restituita da redis: inizio: " + rawJsonTrack.substring(0, 20)
                        + " - fine: " + rawJsonTrack.substring(rawJsonTrack.length() - 20, rawJsonTrack.length() - 1));
            
            } else {
                
                logger.info("Stringa restituita da redis: " + rawJsonTrack);
            
            }
            
            List<Point> results = fromJsonPoints(formatTrack(rawJsonTrack));
            
            logger.info("Restituzione di punti " + results.size() + " di questa traccia " + trackId);
            
            return results;
        
        } else {
            
            logger.info("Non sono stati trovati punti su redis per questa traccia " + trackId);
            
            return null;
        
        }
    
    }

    /**
     * Adds the given points to the specified Realtime Track.
     * 
     * @param trackId the realtime Track id.
     * @param points the points that has to be added to the Track.
     */
    public void addPointToTrack(String trackId, Collection<Point> points) {
        
        if (points.size() > 0) {
            
            StringBuilder jsonPoints = new StringBuilder(toJsonPoints(points));
            jsonPoints.deleteCharAt(0).deleteCharAt(jsonPoints.length() - 1);
            
            logger.info("Aggiunta in realtime su redis di " + points.size() + " punti alla traccia " + trackId);
            
            dbKeyValue.append(preKey + trackId, jsonPoints.toString().trim() + ",");
        
        }
    
    }

    /**
     * Sets the expire time to the points of the specified Realtime Track.
     * 
     * @param trackId the realtime Track id. 
     */
    public void setExpirePoints(String trackId) {
        
        String redisKeyTrack = getRedisKeyTrack(trackId);
        dbKeyValue.expire(redisKeyTrack, hour);
        
        logger.info("Settaggio del tempo di expire per la traccia " + trackId);
    
    }

    /**
     * Checks if the specified Track is present in the Key-Value DB.
     * 
     * @param trackId the id of the searched realtime Track. 
     * @return true if the specified Track is present in the Key-Value DB, otherwise false.
     */
    public boolean existTrack(String trackId) {
        
        return dbKeyValue.exists(trackId);
    
    }

    /**
     * Convert a Track id to the Redis specific format.
     * 
     * @param trackId the id of the Track.
     * @return the Track id converted to the Redis format.
     */
    private String getRedisKeyTrack(String trackId) {
        
        return preKey + trackId;
    
    }

    /**
     * Format a JSON track in a valid format.
     * 
     * @param rawJsonTrack the raw JSON track.
     * @return the JSON track converted in a valid format.
     */
    private String formatTrack(String rawJsonTrack) {
        
        StringBuilder jsonTrackBulder = new StringBuilder(rawJsonTrack);
        if (jsonTrackBulder.substring(0, 1).equals(",")) {
            jsonTrackBulder.deleteCharAt(0);
        }
        
        jsonTrackBulder.insert(0, "[");
        jsonTrackBulder.deleteCharAt(jsonTrackBulder.length() - 1);
        jsonTrackBulder.append("]");
        
        return jsonTrackBulder.toString();
    
    }

    /**
     * Converts the collection of points to JSON.
     * 
     * @param points the points that has to be converted.
     * @return a JSON representation of the collection of points.
     */
    private static String toJsonPoints(Collection<Point> points) {
        
        Collection<Map> mapPoints = new ArrayList<Map>(points.size());
        
        for (Point point : points) {
        
            HashMap<String, String> mapPoint = new HashMap<String, String>(3);
            mapPoint.put("la", point.getLatitude());
            mapPoint.put("lo", point.getLongitude());
            mapPoint.put("t", point.getTime());
            mapPoint.put("e", point.getElevation());
            mapPoints.add(mapPoint);
        
        }
        
        return new Gson().toJson(mapPoints);
    
    }

    /**
     * Converts a JSON string of points to a list.
     * 
     * @param jsonPoints the JSON representation of the points.
     * @return a List of points.
     */
    private static List<Point> fromJsonPoints(String jsonPoints) {
        
        Collection<Map> mapPoints = new Gson().fromJson(jsonPoints, Collection.class);
        List<Point> points = new LinkedList<Point>();
        
        for (Map mapPoint : mapPoints) {
            points.add(new Point((String) mapPoint.get("la"), (String) mapPoint.get("lo"), (String) mapPoint.get("e"), (String) mapPoint.get("t")));
        }
        
        return points;
    
    }
}
